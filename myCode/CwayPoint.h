/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************
* Filename        :CwayPoint.h
* Author          :Vinu
* Description     :Implements waypoint functionality
* 					provides interface for
* 					get latitude,longitude,name
* 					set latitude,longitude,name
* 					calculateDistance between two waypoints
* 					Prints the current waypoint details
*
*
****************************************************************************/

#ifndef CWAYPOINT_H
#define CWAYPOINT_H


#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <map>
#include <list>
#include <vector>
#include <limits>
using namespace std;

// 52.3667° N, 4.9000° E amsterdam
// 49.8667° N, 8.6500° E Darmstadt
//52.5167° N, 13.3833° E Berlin
//35.6833° N, 139.6833° E TOkio

#define RADIUS_EARTH 6378


//#define SHOWADDRESS	// Macro switch to display address of objects
//#define DEBUGPRINTING	// macro switch for debug prints
#define DEGREE (1)  // Print format control : in degrees
#define MMSS (2)    // Print format control : in degree minute seconds
#define TORADIAN  (M_PI/180)  //   Degree to Radian Conversion factor

#define MERGEMODE (0)
#define REPLACEMODE (1)

class CwayPoint {

private:
    double m_longitude;
    double m_latitude;
    string m_name;

public:

    CwayPoint(std::string="default" , double=0.0,double=0.0);


   virtual  CwayPoint*  clone()const;


    string getName()const;

    double getLatitude()const;

    void set(string name, double latitude, double longitude);

    double getLongitude()const;

    void getAllDataByReferance(string &name, double &latitude, double &logitude)const;

    double calculateDistance(const CwayPoint &wp);

    virtual void print(int format);
    friend ostream &operator<<(ostream &output,const CwayPoint &wp);
virtual ~CwayPoint();
    /**
     * @stereotype constructor 
     */
protected:
void transformLongitude2degmmss(int& deg, int &mm, double &ss)const;

  void transformLatitude2degmmss(int& deg, int &mm, double &ss)const;


 };
/********************
**  CLASS END
*********************/
#endif /* CWAYPOINT_H */
