/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************
* Filename        :CGPSSensor.h
* Author          :Vinu
* Description     :Implements GPS sensor functionality.
* 					provides interface for getting the current location via keyboard
*
*
****************************************************************************/

#ifndef CGPSSENSOR_H
#define CGPSSENSOR_H
#include "CwayPoint.h"
class CwayPoint;
/**
 *
 */
class CGPSSensor {
public:
	  CGPSSensor();

     CwayPoint getCurrentPostion();


};
/********************
**  CLASS END
*********************/
#endif /* CGPSSENSOR_H */
