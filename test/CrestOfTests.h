/*
 * CrestOfTests.h
 *
 *  Created on: Jan 20, 2016
 *      Author: vinu
 */

#ifndef CRESTOFTESTS_H_
#define CRESTOFTESTS_H_
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include "CwayPoint.h"
#include "CRoute.h"
using namespace CppUnit;

class CrestOfTests:public TestFixture
{


	CRoute route;

	CwayPoint testwp,Berlin,Amsterdam;
	CPOI mitte,Kinopolis,testPoi;

	CPoiDatabase PoiDatabase;			// Database for navigation

	CwpDatabase WpDataBase;


public:


	void setUp()
	{

		//Do the setup required before each testing
		testwp= CwayPoint("dummy",10,10);
		Berlin=CwayPoint("Berlin",52.5167,13.4);
		Amsterdam=CwayPoint("Amsterdam",52.3667,4.9000);
		mitte=CPOI(RESTAURANT,"mitte","Regular Place",49.87477, 8.656423);
		Kinopolis=CPOI(KINO,"Kinopolis","best german movies",49.8717537,9.6331035);
		testPoi=CPOI(DEFAULT,"dummy","test test",1,1);
		WpDataBase.addWaypoint(Berlin);
		WpDataBase.addWaypoint(Amsterdam);
		WpDataBase.addWaypoint(testwp);

		PoiDatabase.addPoi(Kinopolis);
		PoiDatabase.addPoi(mitte);
		PoiDatabase.addPoi(testPoi);
	}
	/**
	 * cleanup operations after each testcase
	 */
	void tearDown()
	{
		// cleanup operations after each testcase
	}
	/**
	 * test ClearRoute() function
	 * first construct a route and check size non zero
	 * clear and check size 0
	 */
	void testClearRoute()
	{
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route.addWaypoint("Amsterdam");
		route.addWaypoint("Berlin");
		route.addWaypoint("dummy");
		route.addPoi("Kinopolis","Amsterdam");
		route.addPoi("mitte","Amsterdam");
		route.addPoi("dummy","Amsterdam");
		const vector<const CwayPoint*> testrouteObjects1=	route.getRoute();
		int	sizebeforeAdd=testrouteObjects1.size();

		CPPUNIT_ASSERT(sizebeforeAdd==6);
		route.ClearRoute();
		const vector<const CwayPoint*> testrouteObjects=	route.getRoute();
		int	sizeAfterAdd=testrouteObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==0);
	}
	/**
	 * Check distance between two known coordinates to verify getDistanceNextPoi()
	 */
	void testDistanceCalculation(){
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route.addWaypoint("Amsterdam");
		route.addWaypoint("Berlin");
		route.addWaypoint("dummy");
		route.addPoi("Kinopolis","Amsterdam");
		route.addPoi("mitte","Amsterdam");
		route.addPoi("dummy","Amsterdam"); // Coordinate(1,1);

		CwayPoint CurrentLoc= CwayPoint("current",0.0,0.0);// Coordinate(0,0);
		CPOI  *nearestPOI =new CPOI ;

		int	distanceNear=route.getDistanceNextPoi(CurrentLoc,nearestPOI);

		CPPUNIT_ASSERT(157==distanceNear);
		CPPUNIT_ASSERT(nearestPOI->getName()=="dummy");
		//clear the route and repeat test
		route.ClearRoute();
		double	distanceNear1=route.getDistanceNextPoi(CurrentLoc,nearestPOI);
		CPPUNIT_ASSERT(distanceNear1-numeric_limits<double>::max()<0.0000000001); // double connot be compared for equlity because of rounidng errors
		//CPPUNIT_ASSERT(157==distanceNear);

		delete nearestPOI;
	}
	/**
	 * print test.
	 * print console to string stream and compared with sample output string
	 *
	 */
	void CtestPrint()
	{
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route+="mitte";
		route+="Amsterdam";
		std::stringstream stringstr;
		std::streambuf *old_buffCout;

		old_buffCout = std::cout.rdbuf(stringstr.rdbuf());//change currnet  buffer

		route.print();
		std::cout.rdbuf(old_buffCout); //reset
		string console=stringstr.str(),test;

		test="\nPrinting Route \n"
				"There are 2 WP/POI in route\n"
				"1. mitte @ (49� 52' 29.172\" ,8� 39' 23.1228\" )\n"
				"is a : RESTAURANT - \" Regular Place \"\n\n"
				"2. Amsterdam @ (52� 22' 0.12\" ,4� 54' 1.27898e-012\" )\n\n";

		//cout<<console.length()<<" "<<test.length();
		CPPUNIT_ASSERT(console==test);

	}

/**
 * test route addition and copy constructor
 */
	void testrouteAddition(){
		//create a dummy route
		CRoute testRoute;
		testRoute.connectToPoiDB(&PoiDatabase);
		testRoute.connectToWpDB(&WpDataBase);
		testRoute.addWaypoint("Amsterdam");
		const vector<const CwayPoint*> testrouteObjects=	testRoute.getRoute();
		int	sizeAfterAdd=testrouteObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==1);
		CPPUNIT_ASSERT(testrouteObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(testrouteObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(testrouteObjects[0]->getLongitude()==4.9000);
		// another route
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route+="mitte";
// sum the route
		CRoute SumRoute=testRoute+route;
		const vector<const CwayPoint*> SumrouteObjects=	SumRoute.getRoute();
		int 	sizeAfterAdd2=SumrouteObjects.size();
		CPPUNIT_ASSERT(sizeAfterAdd2==2);
		CPPUNIT_ASSERT(SumrouteObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(SumrouteObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(SumrouteObjects[0]->getLongitude()==4.9000);
		CPPUNIT_ASSERT(SumrouteObjects[1]->getName()=="mitte");
		// adding two route with different databases
		CRoute Dummy;
		CRoute NullRoute=SumRoute+Dummy;
		const vector<const CwayPoint*> SumrouteObjectsNull=	NullRoute.getRoute();
		int 	sizeAfterAdd3=SumrouteObjectsNull.size();
		CPPUNIT_ASSERT(sizeAfterAdd3==0);

	}


	static TestSuite* suite() {
		CppUnit::TestSuite* suite = new CppUnit::TestSuite("Croute testing");
		suite->addTest(new CppUnit::TestCaller<CrestOfTests>
		("CtestPrint", &CrestOfTests::CtestPrint));
		suite->addTest(new CppUnit::TestCaller<CrestOfTests>
		("testrouteAddition", &CrestOfTests::testrouteAddition));
		suite->addTest(new CppUnit::TestCaller<CrestOfTests>
		("testDistanceCalculation", &CrestOfTests::testDistanceCalculation));
		suite->addTest(new CppUnit::TestCaller<CrestOfTests>
		("testClearRoute", &CrestOfTests::testClearRoute));

		return suite;
	}
};
#endif /* CRESTOFTESTS_H_ */
