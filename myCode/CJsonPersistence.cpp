/*
 * CJsonPersistence.cpp
 *
 *  Created on: Jan 3, 2016
 *      Author: vinu
 */

#include "CJsonPersistence.h"

using namespace APT;


CJsonPersistence::CJsonPersistence()
{
	// TODO Auto-generated constructor stub

}
/**
 * Sets the base name of files which will be read or written later.
 * file names will be
 * basename.json
 *
 * param name  	: base file name string
 */
void CJsonPersistence::setMediaName(string name)
{
	if(name.empty() == true ||(name.find_first_not_of(' ') == std::string::npos))
	{
		cout<<"ERROR: CfileStorage::setMediaName(string "<<name<<") : Invalid name"<<endl;
		exit(0);
	}
	else
	{
		m_fileName=name;	// assign the name to member varaible
	}
}
/**
 * Writes waypoint and POI database  data to files
 * param waypointDb	: reference to CwpDatabase object from which data is writtedn to file
 * param poiDb			: reference to CPoiDatabase object from which data is writtedn to file
 * return				: returns true if file read is succesful else false
 */
bool CJsonPersistence::writeData(const CwpDatabase & waypointDb,
		const CPoiDatabase & poiDb)
{
	ofstream jsonFile; 								// stream objects
	bool writeStatus=false; 						// write status
	jsonFile.open ((m_fileName+".json").c_str(), ios::out); // file is opened , c_str() function is used to convert string to Char *
	if (jsonFile.is_open())
	{

		map<string,CwayPoint> Wplocations=waypointDb.getDatabase(); 	// get maps containig the objects from the database class
		map<string,CPOI> Poilocations= poiDb.getDatabase();
		if(Wplocations.size()>0)
		{
			jsonFile<<"{\n";											// write initial data
			jsonFile<<"\"waypoints\": [\n";
			printDatabaseToStream(jsonFile,Wplocations);				// call the template function to write Wp info
			jsonFile<<"],\n";
		}

		if(Poilocations.size()>0)
		{
			jsonFile<<"\"pois\": [\n";
			printDatabaseToStream(jsonFile,Poilocations);
			jsonFile<<"]\n";
			jsonFile<<"}";
		}
		writeStatus=true; 										// writing ok
		jsonFile.close();
	}
	else 														// writing failed
	{
		writeStatus=false;
		//cout << "Unable to open file "<<m_fileName+"-wp.csv"<<endl;
	}


	return (writeStatus); 				// return true if both writes  are succesful


}
/**
 * Function reads wp and pois from file filename.json"
 *
 * and saves the valid data to the databases
 * param waypointDb		: reference to CwpDatabase
 * param poiDb				: reference to CPoiDatabase
 *
 * return					: returns true if read is successful else false
 */
bool CJsonPersistence::readData(CwpDatabase & waypointDb, CPoiDatabase& poiDb,
		CPersistentStorage::MergeMode mode)
{
	char buffer[100]; 								// Buffer for itoa function
	bool readStatus=false,invalidCharFlag=false; 	//flags							// read status
	string 		error,								//  variable to store error info string
	attribute, 										// to save read attribute
	name,											// name read from file
	type,											// variable to store the data read from file
	description										// variable to store the data read from file
	;
	double latitude,longitude;						// values from file
	t_poi  poiType;									// type of POi
	ifstream jsonFile;								// file stream
	int errorline,									// store line no
	lastErrorLine=-1,							// last error line
	primeCheckSum=RESET_CS;						// Initial value of check sum =1
	CJsonToken::TokenType currentToken;
	CJsonToken *myToken;
	parserStates state=IDLE;					// current state of state machine
	parserStates goTostate=IDLE;				// state to be changed in seperator state
	parserDBSubstate DBsubstate=NONE;				// state initialization DB info

	if(mode==REPLACE)
	{
		waypointDb.clear();						// in replace mode clear the database first then add the data.
		poiDb.clear();
	}
	jsonFile.open((m_fileName+".json").c_str());				// open the file
	cout<<"Reading file "+(m_fileName+".json")<<endl;
	if (jsonFile.is_open())										// if file is open do the  parsing
	{
		CJsonScanner scanner(jsonFile);							// initialize the scanner

		try
		{
			myToken=scanner.nextToken();						// load the first token
		}
		catch (string& Exception)								// handle exceptions
		{
			error=Exception;
			errorline=scanner.scannedLine();
			cout<<"Read error : illegal character "<<Exception<<endl;

			invalidCharFlag=true;
		}

		while (myToken!=0)								// loop until the end of the file
		{
			//cout<<myToken->str()<<endl;
			if(invalidCharFlag==false)					//skip the state machine after exception occurs(invalid event)
			{
				//cout<<"Line Scanned "<<scanner.scannedLine()<<endl;
				currentToken=myToken->getType();		// load the event name : token type
				//cout<<"Event : "<<currentToken<<endl;
				switch(state)
				{
				/**
				 *  State : IDLE
				 *  Initial state of State machine
				 * Waits for BEGIN_OBJECT event to happen and transition to state WAIT_DBNAME
				 * else report json error.
				 */
				case IDLE:
				{//cout<<"IDLE"<<endl;
					if(currentToken == CJsonToken::BEGIN_OBJECT)
					{
						state=WAIT_DBNAME;							// go to WAIT_DBNAME
					}
					else
					{												// go to DATA_ERROR
						state=DATA_ERROR;
						error="invalid json";
						errorline=scanner.scannedLine();
					}

					break;
				}
				/**
				 * State : WAIT_DBNAME
				 *	Check and Save the database name.
				 *	Report Error if no valid database name is found
				 */
				case WAIT_DBNAME:
				{
					//cout<<"WAIT_DBNAME"<<endl;
					if(currentToken==CJsonToken::STRING)
					{
						if(((CJsonStringToken*)myToken)->getValue()=="waypoints")
						{

							DBsubstate=WAYPOINTSTART;
							goTostate = WAIT_ARRAY;
							state=WAIT_SEPERATOR;
						}
						else if (((CJsonStringToken*)myToken)->getValue()=="pois")
						{
							DBsubstate=POISTART;
							goTostate = WAIT_ARRAY;
							state=WAIT_SEPERATOR;
						}
						else
						{
							error= "Invalid database name";
							state=DATA_ERROR;  // go back waiting for DB name
							errorline=scanner.scannedLine();
						}
					}
					else
					{												// go to DATA_ERROR
						state=DATA_ERROR;
						error="invalid json";
						errorline=scanner.scannedLine();
					}

					break;
				}
				/**
				 * State : DATA_ERROR
				 * 	Handles the Errors reported from various states.
				 * 	Prints the Error codes.
				 * 	Checks for a valid event and changes State
				 */
				case DATA_ERROR:
				{
					//cout<<"DATA_ERROR"<<endl;

					primeCheckSum=RESET_CS;			// Reset the data check sum when error is reported
					if(lastErrorLine!=errorline)	// stop reprinting error every time
					{
						cout << "Read error : "<< error<<"at line "<<errorline<<endl;
						lastErrorLine=errorline;
					}

					if(currentToken==CJsonToken::BEGIN_OBJECT)	// Start of Object Switch to WAIT_ATTRIBUTE to process
					{
						state=WAIT_ATTRIBUTE;

					}
					else if(currentToken==CJsonToken::BEGIN_ARRAY)// Start of Array
					{

						state=WAIT_OBJECT;
					}
					else if(currentToken==CJsonToken::END_ARRAY)	// End of an array
					{
						state=WAIT_SEPERATOR;
						goTostate=WAIT_DBNAME;						// in WAIT_SEPERATOR state , next state will be switched to WAIT_DBNAME
					}
					//				else if(currentToken==CJsonToken::STRING)
					//				{
					//					state=	WAIT_ATTRIBUTE;
					//
					//				}
					break;
				}
				/**
				 * State : WAIT_ARRAY
				 * Waits for BEGIN_ARRAY to switch state to WAIT_OBJECT
				 * if Array Start not found BEGIN_OBJECT happens continue parsing the object
				 * else report error
				 */
				case WAIT_ARRAY:
				{
					//cout<<"WAIT_ARRAY"<<endl;
					if(currentToken==CJsonToken::BEGIN_ARRAY){

						state=WAIT_OBJECT;
					}
					else if(currentToken==CJsonToken::BEGIN_OBJECT){
						state=WAIT_ATTRIBUTE;
					}
					else
					{
						error= "Invalid json format";
						state=DATA_ERROR;  // go back waiting for valid event
						errorline=scanner.scannedLine();

					}
					break;
				}
				/**
				 * State : SAVE_OBJECT
				 * 	add the data to object  Databases.
				 * 	do data validation and report error.
				 * Check for valid events and switch state
				 */
				case SAVE_OBJECT:
				{
					//cout<<"SAVE_OBJECT  "<<primeCheckSum<<endl;

					if(DBsubstate==WAYPOINTSTART&& primeCheckSum ==WAYPOINT_CS)
					{
						waypointDb.addWaypoint(CwayPoint(name,latitude,longitude));//call the db function to save data to database
					}
					else if(DBsubstate==POISTART&& primeCheckSum ==POI_CS )
					{
						poiDb.addPoi(CPOI(poiType,name,description,latitude,longitude));

					}else if(DBsubstate==NONE)
					{
						cout<<"Invalid database name :check line: "<<errorline<<endl;
					}
					else
					{
						cout<<"Read error : "<<error<<endl;
					}
					primeCheckSum=RESET_CS;	// Reset the Checksum after data is saved
					if(myToken->getType()==CJsonToken::VALUE_SEPARATOR) // go to next object
					{
						state=WAIT_OBJECT;
					}
					else if(myToken->getType()==CJsonToken::END_ARRAY) // in case of array end go on processing the next database name
					{
						DBsubstate =NONE;
						state=WAIT_SEPERATOR;
						goTostate=WAIT_DBNAME;

					}
					else if(myToken->getType()==CJsonToken::BEGIN_OBJECT) // in case of missing name seperator and new object begin processing it
					{
						state=WAIT_ATTRIBUTE;

					}
					else	// report error incase of invalid events
					{
						error= "Invalid json format";
						state=DATA_ERROR;  // go back waiting for valid event
						errorline=scanner.scannedLine();

					}

					break;
				}
				/**
				 * State : WAIT_OBJECT
				 * 		State is entered on start of object.
				 * 		Report Error in case of invalid event.
				 *
				 */
				case WAIT_OBJECT:
				{
					//cout<<"WAIT_OBJECT"<<endl;
					if(currentToken==CJsonToken::BEGIN_OBJECT)
					{
						state=WAIT_ATTRIBUTE;
					}
					else
					{
						error= "Invalid json format";
						state=DATA_ERROR;  // go back waiting for valid event
						errorline=scanner.scannedLine()	;
					}

					break;
				}
				/**
				 * State :	WAIT_ATRVALUE
				 *		This state parses all the attribute values .
				 *		Does validation of attribute values and calculate a checksum.(primeCheckSum is a product of  prime numbers)
				 *		set the error info on invalid attribute values.
				 *		The  attribute info is saved in WAIT_ATTRIBUTE.
				 */
				case WAIT_ATRVALUE:
				{
					//cout<<"WAIT_ATRVALUE"<<endl;

					if (currentToken==CJsonToken::STRING||currentToken==CJsonToken::NUMBER)
					{
						if (attribute=="name")		// parse name string
						{
							name=((CJsonStringToken*)myToken)->getValue();

							goTostate=WAIT_ATTRIBUTE;				// go to WAIT_ATTRIBUTE for the next attribute namae
							state=WAIT_SEPERATOR;
							if( name.empty() == true ||(name.find_first_not_of(' ') == std::string::npos) )	// if name is empty or spaces
							{
								string lineno=itoa (scanner.scannedLine(),buffer,10);
								error= "invalid Name at line no :"+ lineno;			// set the error
							}
							else
							{
								primeCheckSum=primeCheckSum*NAME_CS;					// calculate the name checksum
							}

						}
						else if(attribute=="latitude")
						{
							latitude=((CJsonNumberToken*)myToken)->getValue();
							goTostate=WAIT_ATTRIBUTE;
							state=WAIT_SEPERATOR;// go to WAIT_ATTRIBUTE for the next attribute namae
							if (latitude>=-90 && latitude <=90)  	//   latitude valid values check
							{
								primeCheckSum=primeCheckSum*LATI_CS;
							}
							else
							{
								string lineno=itoa (scanner.scannedLine(),buffer,10);
								error="Invalid latitude value at line no :"+lineno;
							}
						}
						else if(attribute=="longitude")
						{
							longitude=((CJsonNumberToken*)myToken)->getValue();
							goTostate=WAIT_ATTRIBUTE;
							state=WAIT_SEPERATOR;

							if(longitude>=-180 && longitude <=180) 	//   longitude valid values range check
							{
								primeCheckSum=primeCheckSum*LONG_CS;
							}
							else
							{
								string lineno=itoa (scanner.scannedLine(),buffer,10);
								error="Invalid longitude value at line no :"+lineno;

							}
						}
						else if(attribute=="type")
						{
							type=((CJsonStringToken*)myToken)->getValue();
							goTostate=WAIT_ATTRIBUTE;
							state=WAIT_SEPERATOR;
							poiType=t_poiToString(type);	// convert the string type to a valid poi type
							if(poiType==INVALID_POI_TYPE)
							{
								string lineno=itoa (scanner.scannedLine(),buffer,10);
								error="Invalid POI type at line No :"+lineno;

							}
							else{

								primeCheckSum=primeCheckSum*TYPE_CS;
							}
						}
						else if(attribute=="description")
						{
							description=((CJsonStringToken*)myToken)->getValue();
							goTostate=WAIT_ATTRIBUTE;
							state=WAIT_SEPERATOR;

							if( description.empty() == true ||(description.find_first_not_of(' ') == std::string::npos) )	// if name is empty or spaces
							{
								string lineno=itoa (scanner.scannedLine(),buffer,10);
								error= "invalid Description at line no :"+ lineno;			// set the error
							}
							else
							{

								primeCheckSum=primeCheckSum*DESC_CS;
							}
						}
						else
						{
							state=DATA_ERROR;
							errorline=scanner.scannedLine();

							error="invalid attribute ";

						}
					}else if(myToken->getType()==CJsonToken::END_OBJECT)
					{
						state=SAVE_OBJECT;

					}
					else
					{
						state=DATA_ERROR;
						error="invalid json";
						errorline=scanner.scannedLine();
					}
					break;
				}
				/**
				 * State : WAIT_ATTRIBUTE
				 * 		Save the attribute for further processing in state WAIT_ATRVALUE
				 * 		in case of object end, State is changed to SAVE_OBJECT.
				 * 		else go to  error state
				 */
				case WAIT_ATTRIBUTE:
				{cout<<"WAIT_ATTRIBUTE "<<endl;
					if(myToken->getType()==CJsonToken::STRING)
					{
						attribute=((CJsonStringToken*)myToken)->getValue();
						//cout<<"WAIT_ATTRIBUTE "<<attribute<<endl;
						state=WAIT_SEPERATOR;
						goTostate=WAIT_ATRVALUE;
					}
					else if(myToken->getType()==CJsonToken::END_OBJECT)
					{
						state=SAVE_OBJECT;

					}
					else
					{
						state=DATA_ERROR;
						error="invalid json";
						errorline=scanner.scannedLine();
					}
					break;

				}
				/**
				 * State : WAIT_SEPERATOR
				 *	 This state handles both NAME_SEPARATOR and VALUE_SEPARATOR events
				 *	 here the pre set goTostate is made the current state.
				 *	 and in case of object end SAVE_OBJECT state transition is made.
				 *	 other events cause error state transition
				 */
				case WAIT_SEPERATOR:
				{
					cout<<"WAIT_SEPERATOR "<<endl;

					if(myToken->getType()==CJsonToken::NAME_SEPARATOR||myToken->getType()==CJsonToken::VALUE_SEPARATOR)
					{
						state=goTostate;
					}
					else if(myToken->getType()==CJsonToken::END_OBJECT)
					{
						state=SAVE_OBJECT;

					}
					else if(myToken->getType()==CJsonToken::BEGIN_ARRAY)
					{
						state=WAIT_OBJECT;
					}

					else
					{
						state=DATA_ERROR;
						errorline=scanner.scannedLine();
						error="invalid json format ";
					}

					break;
				}


				};
			}//end of invalid char check
			invalidCharFlag=false;	// reset error flag before next token is loaded.

			try
			{
				myToken=scanner.nextToken();	// load next token
			}
			catch (string& Exception) {

				cout<<"Read error : illegal character "<<Exception<<endl;

				invalidCharFlag=true;		// set error flag in case of error.
			}
		}
		readStatus=true;
	}
	else
	{
		readStatus=false;
	}
	jsonFile.close(); 											// close the file

	return readStatus;
}

CJsonPersistence::~CJsonPersistence()
{
	// TODO Auto-generated destructor stub
}
/**
 * Prints Waypoint specific part
 * param char_traits : STREAM object
 * param itr_map	: iterator containg the valid object
 */
void printData(ofstream& char_traits, map<string,CwayPoint> ::const_iterator itr_map ){

	char_traits<<"\n";		// next line since no more data is left for printing to stram
}
/**
 * Print poi specific part
 * param char_traits : STREAM object
 * param itr_map	: iterator containg the valid object
 */
void printData(ofstream& char_traits,map<string,CPOI> ::const_iterator itr_map ){

	char_traits<<",\n";
	char_traits<<"\"type\": \""<<(itr_map->second).getType()<<"\",\n"
			<<"\"description\": \""<<(itr_map->second).getDescription()<<"\"\n";
}

template <typename T1>  void CJsonPersistence::printDatabaseToStream(ofstream& char_traits, map<string,T1 > poimap)
{
	typename map<string,T1> ::const_iterator itr_map, checkitr=poimap.end(); // a constant iterator is required since const functions are called through the iterator
	checkitr--;																	// iterator to last element

	for (itr_map=poimap.begin();itr_map != poimap.end();itr_map++) 		// traversing the map using iterator
	{
		char_traits<<"{\n";

		char_traits
		<<"\"name\": \""<<(itr_map->second).getName()<<"\",\n"
		<<"\"latitude\": "<<(itr_map->second).getLatitude()<<",\n"
		<<"\"longitude\": "<<(itr_map->second).getLongitude();
		printData(char_traits,itr_map); 			// overloaded function correct function will be called based on the type of itr_map

		if (checkitr==itr_map)		// when last object has been printed terminate the object json.
		{
			char_traits <<"}\n";
		}
		else
		{

			char_traits <<"},\n";	// more objects to follow so }, printed
		}
	}



}


