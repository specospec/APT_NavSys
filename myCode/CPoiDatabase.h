/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************
* Filename        :
* Author          :
* Description     :
*
*
****************************************************************************/

#ifndef CPOIDATABASE_H
#define CPOIDATABASE_H
#include "CPOI.h"
#include "CtemplateDatabase.h"
using namespace APT;
#define NOMAXPOIDB 10

class CPoiDatabase : private CtemplateDatabase<string,CPOI>
{
private:
    /**
     * @link aggregationByValue 
     */
//map<string,CPOI> m_mapPOI; // map containing the CPOI objects stored with name as their key

public:

    CPoiDatabase(); // default constructor

    void addPoi(const CPOI &poi); // Add POI to DB
    void clear();
    CPOI* getPointerToPoi(string name); // Query and return pointer to POI
	map<string,CPOI> getDatabase(void)const;
    //void printPOIDatabaseToStream(ofstream &)const;

   // void print();
};
/********************
**  CLASS END
*********************/
#endif /* CPOIDATABASE_H */
