/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************/

#include "CfileStorage.h"

/*
 * Array of strings to parse error enum into strings
 */
string t_parseErrorString[]={"NOERROR","TYPE_INVALID","NAME_INVALID","DESCRIPTION_INVALID","LATITUDE_INVALID","LONGITUDE_INVALID","PARAMETER_COUNT_INVALID"};


/**
 *convertes t_poi to corresponding type
 * @param type : string which is read from file to be converted to type t_poi
 * @return		: returns the converted string else returns INVALID_POI_TYPE
 */
t_poi t_poiToString(string type)
{
	int i=0,flag=0;

	for(i=T_POI_COUNT-1;i>0;i--) 	// iterate through the array containing the POI types and check if it is equal to string type
	{
		if(pois[i]==type)  			// incase poi type is found set flag and break from the loop.
		{
			flag=1;
			break;
		}

	}
	if(flag==1)						// when t_poi string is found in the array return the t_tpoi
	{
		return (t_poi)i;
	}
	else return INVALID_POI_TYPE; 	// else return INVALID_POI_TYPE
}

/**
 * Function to fix comma in latitude and longitude data
 * @param number : the reference to  latitude and longitude strings
 */
void fixComma(string &number){

	unsigned int pos=number.find(",");  // checks the string for ","  stores position in pos
	if ( pos != string::npos)			// if "," is not found (pos=string::npos)
	{
		number.replace(pos,1,".");		// replace the "," with "."
	}

}
/**
 * Checks whether the stirng can be succsfully converted to float.
 * returns false when
 * non float value is parsed.
 * mixed data is parsed (eg. 12abc)
 * Reason why i wrote this
 * atof()and strtod() returns 0(false) incase of non float value and when converting valid parameters like 0 and 0.0
 * so a workaround is done in this function so that 0 and 0.0 are accepted.
 *
 * @param parameterTemp : string to be converted to double
 * @return : true if a valid no else false
 */
bool isValidNumber(string parameterTemp){
double num;
bool temp=true;
	char* pEnd=NULL;
	fixComma(parameterTemp);	// fix comma otherwise the pEnd will point to numbers after the comma
	num=strtod(parameterTemp.c_str(),&pEnd);
	temp=(bool)num;

	if(num==false) // incase of a unsuccessful conversion or occurrence of 0/0.0
	{
		 temp = (	false == parameterTemp.compare("0") || false ==parameterTemp.compare("0.0")); // compare returns 0 on succesful compare
	}
	else if ((*pEnd!=NULL))  // pEnd will point to non integer part of string after parsing mixed input like 10.0text
	{
		temp=false;			// not a correct conversion as data itself is invalid
	}

#ifdef DEBUGPRINTING

	cout<<parameterTemp<<" isValidNumber 0 "<<parameterTemp.compare("0")<<" 0.0 "<<parameterTemp.compare("0.0")<<endl;
	cout<<"isValidNumber strtod "<<num<<endl;
	cout<<"isValidNumber pointer "<<(*pEnd==NULL)<<endl;
	cout<<"isValidNumber  temp = "<<temp<<endl;
#endif

	return temp;
}
/**
 * Sets the base name of files which will be read or written later.
 * file names will be
 * basename-poi.csv
 * basename-poi.csv
 * @param name  	: base file name string
 */
void CfileStorage::setMediaName(string name)
{
	if(name.empty() == true ||(name.find_first_not_of(' ') == std::string::npos))
	{
		cout<<"ERROR: CfileStorage::setMediaName(string "<<name<<") : Invalid name"<<endl;
	exit(0);
	}
	else
	{
		m_fileName=name;	// assign the name to member varaible
	}
}

/**
 * Writes waypoint and POI database  data to files
 * @param waypointDb	: reference to CwpDatabase object from which data is writtedn to file
 * @param poiDb			: reference to CPoiDatabase object from which data is writtedn to file
 * @return				: returns true if file read is succesful else false
 */
bool CfileStorage::writeData(const CwpDatabase& waypointDb,
		const CPoiDatabase& poiDb)
{
	ofstream wpfile,poifile; 								// stream objects
	bool writeStatusWp=false,writeStatusPoi=false;; 		// write status
	wpfile.open ((m_fileName+"-wp.csv").c_str(), ios::out); // file is opened , c_str() function is used to convert string to Char *
	if (wpfile.is_open())
		{
														//pass the stream object to CwpDatabase member fucntion to write data to stream
		printWaypointDatabaseToStream(waypointDb.getDatabase(),wpfile);

		writeStatusWp=true; 								// writing ok
		wpfile.close();
		}
	else 													// writing failed
	{
		writeStatusWp=false;
		cout << "Unable to open file "<<m_fileName+"-wp.csv"<<endl;
	}
															// same process is repeated for writing the poidatabase
	poifile.open ((m_fileName+"-poi.csv").c_str(), ios::out);
	if (poifile.is_open())
		{
															//pass the stream object to poidatabase member function to write data to stream

		printPOIDatabaseToStream(poiDb.getDatabase(),poifile);

		writeStatusPoi=true;
		poifile.close();
		}
	else
	{
		writeStatusPoi=false;
		cout << "Unable to open file "<<m_fileName+"-poi.csv"<<endl;
	}


	return (writeStatusPoi&&writeStatusWp); 				// return true if both writes  are succesful
}
/**
 * Function reads wp and pois from files m_fileName+"-wp.csv" & m_fileName+"-poi.csv"
 * data delimiter is semicolon ;
 * and saves the valid data to the databases
 * @param waypointDb		: reference to CwpDatabase
 * @param poiDb				: reference to CPoiDatabase
 * @param mode				: MERGE or REPLACE mode
 * @return					: returns true if read is successful else false
 */
bool CfileStorage::readData(CwpDatabase& waypointDb, CPoiDatabase& poiDb,
		MergeMode mode)
{

	bool readStatusWp=false,readStatusPoi=false; // read status
	if(mode==REPLACE)
	{
		waypointDb.clear();						// in replace mode clear the databawse first then add the data.
		poiDb.clear();
	}
	readStatusWp= readDataWayPoint(waypointDb); // read the waypoint file
	readStatusPoi= readDataPOI (poiDb);			// read the POI file

	 return (readStatusWp&&readStatusPoi);

}
/**
 * function to read waypoint data from file
 * @param waypointDb		: reference to CwpDatabase
 * @param poiDb				: reference to CPoiDatabase
 * @param mode				: MERGE or REPLACE mode
 * @return					: return read status
 */
bool CfileStorage::readDataWayPoint(CwpDatabase& waypointDb		)
{
	bool readStatus;							// read status
	string 		line,							//  variable to store one line read from file
				lineBak, 						// to save the backup of a line
				name,							// variable to store the data read from file
				description,					// variable to store the data read from file
				parameterTemp; 					//  local string array for saving each substring generated from the line for furthur processing
	ifstream wpfile;							// ifstream object  for reading data

	int lineNo=1;								// initial line no
	unsigned int lastPos=0,firstPos=0; 			// position varibale store the start and end of substrings
	t_parseError wpParseError=NOERROR;			// error status enum
	double latitude,logitude;					// variable to store the data read from file
	cout<<"Parsing "+(m_fileName+"-wp.csv")<<endl;

	wpfile.open((m_fileName+"-wp.csv").c_str());				// open the file

	if (wpfile.is_open())										// if file is open do the  parsing
	{
		while ( getline (wpfile,line) )							// load each line from the file till end of file
		{
			lineBak=line;
			wpParseError=NOERROR;								// initially no r=error

			for(int i=0;i<=NUM_ADDWP_PARAMS-1;i++)				// check each line NUM_ADDWP_PARAMS no of times to read each property
			{
				firstPos=0;										// initial pos 0;
				lastPos= line.find(";");						// find the position of ';' in the file
				if(lastPos==string::npos)						//if ; is not found assume that now processing the last info "logitude " which comes after last ; in a line
				{
					lastPos=line.size();						// last position is then the size of the substring
				}
				parameterTemp= line.substr(firstPos,lastPos);	// string line is split into substrings using the calculates start end positions and stored to the array

				line.erase(firstPos,++lastPos);					// erase the stored part from the line including the ;

				switch(i)										// switch to process each parameter
				{
			case 0:												//when  i=0  name is stored and processed here
				{
					if( parameterTemp.empty() == true ||(parameterTemp.find_first_not_of(' ') == std::string::npos) )	// if name is empty or spaces
					{
						wpParseError=NAME_INVALID;				// set the error
					}
					else
					{
						name=parameterTemp;						// else store the processed data to string name
					}
					break;
				}
				case 1:											// for i=1 second parameter latitude is processed
				{
					if(parameterTemp.empty() == true ||  isValidNumber(parameterTemp) ==false  ) // check the string is not empty and is a valid number
					{
						wpParseError=LATITUDE_INVALID;			// set the error
					}
					else
					{
						fixComma(parameterTemp); 				// change  ',' to '.' if it is there in string

						latitude=atof(parameterTemp.c_str()); 	// convert the string to double and save
					}


					break;
				}
				case 2:
				{
					if( parameterTemp.empty()== true) 			// if the string is empty set the error
					{
																//parameterList will be empty if the line has too few atributs
						wpParseError=PARAMETER_COUNT_INVALID;	//  set the error

					}
					else if( isValidNumber(parameterTemp) ==false )	// if the string cannot be converted to a valid no set the error
					{

						wpParseError=LONGITUDE_INVALID;
					}
					else										// if no error has occured set the value of longitude
					{
						fixComma(parameterTemp);

					logitude=atof(parameterTemp.c_str());
					}

					break;
				}
				default :{
																	//code never reached
				}
				};
			}

			if(wpParseError==NOERROR) 								// check if no error is there
			{

				waypointDb.addWaypoint(CwayPoint(name,latitude,logitude));

			}
			else 													// incase of error print the error no and line no;
			{
				cout<<"Error parsing line : "<<lineNo<<" Error ID : "<<t_parseErrorString[wpParseError]<<" Line = "<<lineBak<<endl;
			}

			lineNo++; 												// increment the line after processing is done
		}
		wpfile.close(); 											// close the file
	readStatus=true; 												// set the read status to true
	}

	else 															// not able to read the file
	{
		cout << "Unable to open file "<<m_fileName+"-wp.csv"<<endl;
		readStatus=0;
	}
	return readStatus;
}

/**
 * Reads the POI from file and add the valid POI to database
 * this functon does the error handling while reading the data
 * @param poiDb		: point of interest database.
 * @return			:  read status
 */
bool CfileStorage::readDataPOI(CPoiDatabase& poiDb)
{
	bool readStatus;								// read status
	string  line,									// variable to store the data read from file
			lineBak, 								// to save the backup of a line
			name, 									//  variable to store one line read from file
			description,							//variable to store description of poi
			parameterTemp; 							//  local string array for saving each substring generated from the line for furthur processing
	ifstream poifile;								// ifstream object  for reading data
	t_poi type;										//  variable to store poi type read from file
	int lineNo=1;									// initial line no
	unsigned int lastPos=0,firstPos=0;				// position varibale store the start and end of substrings
	t_parseError poiParseError=NOERROR;				// error status enum
	double latitude,logitude;						// variable to store the data read from file

	cout<<"Parsing "+(m_fileName+"-poi.csv")<<endl;
	poifile.open((m_fileName+"-poi.csv").c_str());	// open the file

	if (poifile.is_open())							// if file is open do the  parsing
	{
		while ( getline (poifile,line) )			// load each line from the file till end of file
		{
			lineBak=line;							// save a backup of the line
			poiParseError=NOERROR;					// initially no error

			for(int i=0;i<=NUM_ADDPOI_PARAMS-1;i++)// check each line NUM_ADDWP_PARAMS(5 times) no of times to read each property, data after that is not processed
			{
				firstPos=0;							// initial pos 0;
				lastPos= line.find(";");			// find the position of ';' in the file
				if(lastPos==string::npos)			//if ; is not found assume that now processing the last info "logitude " which comes after last ; in a line
				{
					lastPos=line.size();			// last position is then the size of the substring
				}
				parameterTemp= line.substr(firstPos,lastPos);// string line is split into substrings using the calculated start end positions and stored to the array
				line.erase(firstPos,++lastPos);		// erase the stored part from the line including the ;

				switch(i)							// switch to process each parameter
				{
				case 0:								//when  i=0  name is stored in parameterList[0] and processed here
				{

					if( parameterTemp.empty() == true || INVALID_POI_TYPE ==  t_poiToString( parameterTemp)) // is string is empty or it is an invalid type set error
					{
						poiParseError=TYPE_INVALID;
					}
					else 										// set the type
					{
						type= t_poiToString( parameterTemp); 	// t_poiToString converts string to a t_poi
					}

					break;
				}
				case 1: 										// check name
				{
					if( parameterTemp.empty() == true||(parameterTemp.find_first_not_of(' ') == std::string::npos)  ) 	// if name is empty set error or spaces
					{
						poiParseError=NAME_INVALID;
					}
					else 										// set the name
					{
						name=parameterTemp;
					}
					break;
				}
				case 2: 										// check description
				{
					if( parameterTemp.empty() == true||(parameterTemp.find_first_not_of(' ') == std::string::npos)  ) // set error when string is empty or spaces
					{
						poiParseError=DESCRIPTION_INVALID;
					}
					else
					{
						description=parameterTemp; 				// set description
					}
					break;
				}
				case 3: 										// check latitude
				{
					if(parameterTemp.empty() == true || isValidNumber(parameterTemp) ==false )
					{
						poiParseError=LATITUDE_INVALID; 		// set error if string is empty or it is not a valid number
					}
					else
					{
						fixComma(parameterTemp); 				// replace ',' with '.'

						latitude=atof(parameterTemp.c_str()); 	// set the latitude
					}


					break;
				}
				case 4: 										// check longitude
				{
					if(parameterTemp.empty()== true)
					{
						poiParseError=PARAMETER_COUNT_INVALID; 	// set error incase of parameter is empty. this becasue of too few parameters

					}
					else if(isValidNumber(parameterTemp) ==false )
					{

						poiParseError=LONGITUDE_INVALID; 		// set error incase it is not a valid string
					}
					else
					{
						fixComma(parameterTemp); 				// fix comma

					logitude=atof(parameterTemp.c_str()); 		//  set the longitude
					}

					break;
				}
				default :{
					cout<<"code never reached"<<endl;
				}
				};

			}
			if(poiParseError==NOERROR)
			{

					poiDb.addPoi(CPOI(type,name,description,latitude,logitude));

			}
			else
			{														// print the error no , error id and the line
				cout<<"Error parsing line : "<<lineNo<<" Error ID : "<<t_parseErrorString[poiParseError]<<" Line : "<<lineBak<<endl;
			}

			lineNo++; 												// increment the line number after each line is processed
		}
		poifile.close();											// close the file
	readStatus=true; 												// all is ok
	}

	else {
		cout << "Unable to open file "<<m_fileName+"-poi.csv"<<endl;
		readStatus=false;
	}
	return readStatus;
}
/**
 * Dummy
 */
CfileStorage::~CfileStorage()
{
}

 void CfileStorage:: printPOIDatabaseToStream(map<string,CPOI> data,ofstream& char_traits) const
{


	map<string,CPOI> ::const_iterator itr_map; // a constant iterator is required since const functions are called through the iterator
	for (itr_map=data.begin();itr_map != data.end();itr_map++) 		// traversing the map using iterator
	{
		char_traits<<(itr_map->second).getType()<<";" 		// get each  properties using CPOI getter functions with returns string
				<<(itr_map->second).getName()<<";"
				<<(itr_map->second).getDescription()<<";"
				<<(itr_map->second).getLatitude()<<";"
				<<(itr_map->second).getLongitude()<<endl;
		//char_traits <<

	}


}

 void CfileStorage:: printWaypointDatabaseToStream(map<string,CwayPoint> data,ofstream& char_traits) const
 {
		map<string,CwayPoint> ::const_iterator itr_map;
		for (itr_map=data.begin();itr_map != data.end();itr_map++)
		{
			char_traits<<(itr_map->second).getName()<<";"<<(itr_map->second).getLatitude()<<";"<<(itr_map->second).getLongitude()<<endl;
			//char_traits <<

		}


 }
/*
 * CfileStorage.cpp
 *
 *  Created on: Nov 27, 2015
 *      Author: vinu
 */




