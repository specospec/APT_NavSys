/*
 * CaddPoi.h
 *
 *  Created on: Jan 15, 2016
 *      Author: vinu
 */

#ifndef CADDPLUSEQUAL_H_
#define CADDPLUSEQUAL_H_
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include "CwayPoint.h"
#include "CRoute.h"
using namespace CppUnit;

class CaddPlusEqual:public TestFixture
{


	CRoute route;

	CwayPoint testwp,Berlin,Amsterdam;
	CPOI mitte,Kinopolis,testPoi;

	CPoiDatabase PoiDatabase;			// Database for navigation

	CwpDatabase WpDataBase;

	int sizeAfterAdd;
public:

	CaddPlusEqual()
{

}
	void setUp()
	{

		//Do the setup required before each testing
		testwp= CwayPoint("dummy",10,10);
		Berlin=CwayPoint("Berlin",52.5167,13.4);
		Amsterdam=CwayPoint("Amsterdam",52.3667,4.9000);
		mitte=CPOI(RESTAURANT,"mitte","Regular Place",49.87477, 8.656423);
		Kinopolis=CPOI(KINO,"Kinopolis","best german movies",49.8717537,9.6331035);
		testPoi=CPOI(DEFAULT,"dummy","blah blah",1.1,1.1);
		WpDataBase.addWaypoint(Berlin);
		WpDataBase.addWaypoint(Amsterdam);
		WpDataBase.addWaypoint(testwp);

		PoiDatabase.addPoi(Kinopolis);
		PoiDatabase.addPoi(mitte);
		PoiDatabase.addPoi(testPoi);
	}
	/**
	 * cleanup operations after each testcase
	 */
	void tearDown()
	{
		// cleanup operations after each testcase
	}
	/**
	 * adding poi before db init
	 */
	void testPlusEqual_DBnoInit(){

		route.ClearRoute();
		route+="mitte";
		route+="mitte";
		const vector<const CwayPoint*> routeObjects =	route.getRoute();
		sizeAfterAdd=routeObjects.size();
		CPPUNIT_ASSERT(sizeAfterAdd==0);
	}
	/**
	 * adding poi after a db init with Null
	 */
	void testPlusEqual_DBInitNULL(){

		route.ClearRoute();
		route.connectToPoiDB(NULL);
		route.connectToWpDB(NULL);
		route+="mitte";
		route.addWaypoint("Darmstadt");
		const vector<const CwayPoint*> routeObjects =	route.getRoute();
		sizeAfterAdd=routeObjects.size();
		CPPUNIT_ASSERT(sizeAfterAdd==0);
	}
	/**
	 * after db init invalid poi and invalid  wp are added
	 */
	void testPlusEqual_invalidPOIorWp() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route+="invalid_name";

		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==0);

	}
	/**
	 * route+="Amsterdam;  AmsterDam not in route
	 */
	void testPlusEqual_invalidPOIvalidWp1() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route+="Amsterdam";
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==1);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);



	}
	/**
	 * route+="Amsterdam"; AmsterDam  in route
	 */
	void testPlusEqual_invalidPOIvalidWp2() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("Amsterdam");
		route+="Amsterdam";
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==1);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);



	}

	/**
	 * route+="mitte";
	 * mitte valid poi not a wp
	 */
	void testPlusEqual_validPOIinvalidWp() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route+="mitte";
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==1);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="mitte");
		CPPUNIT_ASSERT(((CPOI*)routeObjects[0])->getType()=="RESTAURANT");



	}

	void testPlusEqual_validPOIvalidWp() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route+="dummy";
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==2);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="dummy");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==10);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==10);
		CPPUNIT_ASSERT(routeObjects[1]->getName()=="dummy");
		CPPUNIT_ASSERT(((CPOI*)routeObjects[1])->getType()=="DEFAULT");
		CPPUNIT_ASSERT(((CPOI*)routeObjects[1])->getDescription()=="blah blah");
	}

	/**
	 * add duplicate POi
	 * @return
	 */
	void testPlusEqual_validWPDuplication() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route+="Amsterdam";
		route.addPoi("mitte","Amsterdam");
		route+="Amsterdam";
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==3);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);
		CPPUNIT_ASSERT(routeObjects[1]->getName()=="mitte");
		CPPUNIT_ASSERT(((CPOI*)routeObjects[1])->getType()=="RESTAURANT");
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[2]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[2]->getLongitude()==4.9000);
	}

	/**
	 * add duplicate wp back to back
	 * @return
	 */
	void testPlusEqual_validWPDuplication2() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route+="Amsterdam";

		route+="Amsterdam";
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==1);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);
	}


	void testPlusEqual_validpoiDuplication() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route+="mitte";

		route+="mitte";
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==2);

		CPPUNIT_ASSERT(routeObjects[0]->getName()=="mitte");
		CPPUNIT_ASSERT(((CPOI*)routeObjects[0])->getType()=="RESTAURANT");
		CPPUNIT_ASSERT(routeObjects[1]->getName()=="mitte");
		CPPUNIT_ASSERT(((CPOI*)routeObjects[1])->getType()=="RESTAURANT");

	}

	static TestSuite* suite() {
		CppUnit::TestSuite* suite = new CppUnit::TestSuite("Addwp() testing");
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_validWPDuplication2", &CaddPlusEqual::testPlusEqual_validWPDuplication2));
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_DBnoInit", &CaddPlusEqual::testPlusEqual_DBnoInit));
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_invalidPOIorWp", &CaddPlusEqual::testPlusEqual_invalidPOIorWp));
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_invalidPOIvalidWp1", &CaddPlusEqual::testPlusEqual_invalidPOIvalidWp1));
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_invalidPOIvalidWp2", &CaddPlusEqual::testPlusEqual_invalidPOIvalidWp2));
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_validPOIinvalidWp", &CaddPlusEqual::testPlusEqual_validPOIinvalidWp));
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_validPOIvalidWp", &CaddPlusEqual::testPlusEqual_validPOIvalidWp));
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_validWPDuplication", &CaddPlusEqual::testPlusEqual_validWPDuplication));
		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_validpoiDuplication", &CaddPlusEqual::testPlusEqual_validpoiDuplication));

		suite->addTest(new CppUnit::TestCaller<CaddPlusEqual>
		("testPlusEqual_DBInitNULL", &CaddPlusEqual::testPlusEqual_DBInitNULL));
		return suite;
	}
};

#endif /* CADDPLUSEQUAL_H_ */
