/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************
* Filename        :CPOI.h
* Author          :Vinu
* Description     :Class describing Point of interests.
* 					Provides interfaces for
* 					setting the properties description and type.
* 					and get data be call by reference.
* 					This class also has all the public functions of Ccwaypoint
*
*
****************************************************************************/

#ifndef CPOI_H
#define CPOI_H
#include "CwayPoint.h"
/*
 * Point of interest type enumerator
 */

enum t_poi{DEFAULT,RESTAURANT,KINO,GAS,ATM,SIGHTSEEING,PARK,CITYCENTER};
#define T_POI_COUNT (8)

/*
 * Publicly inheriting CwayPoint to access the elements and functions.
 */

class CPOI:public CwayPoint {
private:
    t_poi m_type;
    string m_description;
public:

  CwayPoint* clone()const;
    CPOI(t_poi type=DEFAULT,std::string="default" ,  string description="Default",double=0.0,double=0.0);

    void print(int format); // Print POI

    void set(t_poi,string);
string getType()const;
string getDescription()const;
    void getAllDataByReference(string &name,double &latitude, double &logitude, t_poi& type, string& description)const;
    friend ostream &operator<<(ostream &output,const CPOI &Poi);
};
/********************
**  CLASS END
*********************/
#endif /* CPOI_H */
