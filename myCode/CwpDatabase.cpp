/*
 * CwpDatabase.cpp
 *
 *  Created on: Nov 26, 2015
 *      Author: vinu
 */

#include "CwpDatabase.h"

CwpDatabase::CwpDatabase()
{
	// TODO Auto-generated constructor stub

}
/**
 *returns pointer to waypoint
 * @param allocator
 * @return
 */
CwayPoint* CwpDatabase::getPointerToWaypoint(string allocator)
{
//	if(!m_mapWp.empty())
//	{
//
//		//for (itr_map=m_mapPOI.begin();itr_map != m_mapPOI.end();itr_map++)
//		//{
//			if(m_mapWp.count( allocator ))
//			{
//				return &(m_mapWp[allocator]);
//		//	}
//		}
//	}
	return getPointerToLocation(allocator); // no elements in map or element not found
}
/**
 * accept otput stream object and print data to the stream
 * @param char_traits
 */
//void CwpDatabase::printWaypointDatabaseToStream(ofstream & char_traits)const
//{
//	map<string,CwayPoint> ::const_iterator itr_map;
//	for (itr_map=m_mapWp.begin();itr_map != m_mapWp.end();itr_map++)
//	{
//		char_traits<<(itr_map->second).getName()<<";"<<(itr_map->second).getLatitude()<<";"<<(itr_map->second).getLongitude()<<endl;
//		//char_traits <<
//
//	}
//
//}
/**
 * returns Map containing the objects
 * @return
 */
map<string, CwayPoint> CwpDatabase::getDatabase(void)const
{
	return getDatabaseMap();
}

CwpDatabase::~CwpDatabase()
{
	// TODO Auto-generated destructor stub
}
/**
 * add waypoints to database
 * @param wp	: waypoint object to be added
 */
void CwpDatabase::addWaypoint(const CwayPoint  & wp)
{
addLocation(wp);
//	if (m_mapWp.count( wp.getName() ))
//	{
//		cout<<"CwpDatabase::addWaypoint()Waypoint "<<wp.getName()<<" already present in WP Database "<<endl;
//	}
//
//	else
//	{
//		//cout<<"adding Waypoint  "<<wp.getName()<<" to WP database"<<endl;
//		m_mapWp[wp.getName()]	=wp;										   // element added to DB. Counter incremented
//	}

}
/**
 * Clear the database map
 */
void CwpDatabase::clear()
{
	mapClear();
//	m_mapWp.clear()	;
}
