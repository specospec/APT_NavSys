// GIT-Labor
// main.h

////////////////////////////////////////////////////////////////////////////////
// Header-Dateien
#include <iostream>		// Header f�r die Standard-IO-Objekte (z.B. cout, cin)
#include <stdlib.h>
// TODO: F�gen Sie hier weitere ben�tigte Header-Dateien der
// Standard-Bibliothek ein z.B.
// #include <string>
int count_constructor=0;
#include "CwayPoint.h"
#include "CwpDatabase.h"
#include "CPoiDatabase.h"
#include "CNavigationSystem.h"
#include "CfileStorage.h"
#include "CtestaddWp.h"
#include "CaddPoiTest.h"
#include "CaddPlusEqual.h"
#include "CrestOfTests.h"
#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/ui/text/TestRunner.h>
using namespace std;
using namespace CppUnit;// Erspart den scope vor Objekte der
						// C++-Standard-Bibliothek zu schreiben
						// z.B. statt "std::cout" kann man "cout" schreiben

// Inkludieren Sie hier die Header-Files Ihrer Klassen, z.B.
// #include "CFraction.h"


// Hauptprogramm
// Dient als Testrahmen, von hier aus werden die Klassen aufgerufen
int main (void)
{
    // TODO: F�gen Sie ab hier Ihren Programmcode ein
	cout << "unit Testing" << endl << endl;

	CPPUNIT_NS::TestResult testresult;

	TextUi::TestRunner runner;
	//runner.addTest( new CtestaddWp() );
	 runner.addTest( CtestaddWp::suite() );
	 runner.addTest( CaddPoiTest::suite() );
	 runner.addTest( CaddPlusEqual::suite() );
	 runner.addTest( CrestOfTests::suite() );
	 runner.run();
	return 0;
}
