/*
 * CwpDatabase.h
 *
 *  Created on: Nov 26, 2015
 *      Author: vinu
 */

#ifndef CWPDATABASE_H_
#define CWPDATABASE_H_
#include "CwayPoint.h"
#include "CtemplateDatabase.h"
using namespace APT;
class CwpDatabase: private CtemplateDatabase<string,CwayPoint>
{
//	map<string,CwayPoint> m_mapWp;

public:
	CwpDatabase();
	void addWaypoint(const CwayPoint &wp);
	CwayPoint* getPointerToWaypoint(string);
	void printWaypointDatabaseToStream(ofstream &)const;
	map<string,CwayPoint> getDatabase(void)const;
	void clear();
	~CwpDatabase();
};

#endif /* CWPDATABASE_H_ */
