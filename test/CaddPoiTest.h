/*
 * CaddPoi.h
 *
 *  Created on: Jan 15, 2016
 *      Author: vinu
 */

#ifndef CADDPOI_H_
#define CADDPOI_H_
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include "CwayPoint.h"
#include "CRoute.h"
using namespace CppUnit;

class CaddPoiTest:public TestFixture
{


	CRoute route;

	CwayPoint testwp,Berlin,Amsterdam;
	CPOI mitte,Kinopolis;

	CPoiDatabase PoiDatabase;			// Database for navigation

	CwpDatabase WpDataBase;

	int sizeAfterAdd;
public:

	CaddPoiTest()
{

}
	void setUp()
	{

		//Do the setup required before each testing
		testwp= CwayPoint("dummy",10,10);
		Berlin=CwayPoint("Berlin",52.5167,13.4);
		Amsterdam=CwayPoint("Amsterdam",52.3667,4.9000);
		mitte=CPOI(RESTAURANT,"mitte","Regular Place",49.87477, 8.656423);
		Kinopolis=CPOI(KINO,"Kinopolis","best german movies",49.8717537,9.6331035);

		WpDataBase.addWaypoint(Berlin);
		WpDataBase.addWaypoint(Amsterdam);
		WpDataBase.addWaypoint(testwp);

		PoiDatabase.addPoi(Kinopolis);
		PoiDatabase.addPoi(mitte);

	}
	/**
	 * cleanup operations after each testcase
	 */
	void tearDown()
	{
		// cleanup operations after each testcase
	}
	/**
	 * adding poi before db init
	 */
	void addPoiTest_DBnoInit(){

		route.ClearRoute();
		route.addPoi("mitte","Darmstadt");

		const vector<const CwayPoint*> routeObjects =	route.getRoute();
		sizeAfterAdd=routeObjects.size();
		CPPUNIT_ASSERT(sizeAfterAdd==0);
	}
	/**
	 * after db init invalid poi and invalid  wp are added
	 */
	void testaddPoi_invalidPOI() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addPoi("invalid_name","afterInvalid");

		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==0);
		route.addPoi("invalid_name","Amsterdam");
		const vector<const CwayPoint*> routeObjects1=	route.getRoute();
		sizeAfterAdd=routeObjects1.size();

		CPPUNIT_ASSERT(sizeAfterAdd==0);
	}
	/**
	 * route.addPoi("invalid_name","Amsterdam"); AmsterDam not in route
	 */
	void testaddPoi_invalidPOIvalidWp1() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addPoi("invalid_name","Amsterdam");
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==0);




	}
	/**
	 * route.addPoi("invalid_name","Amsterdam"); AmsterDam  in route
	 */
	void testaddPoi_invalidPOIvalidWp2() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("Amsterdam");
		route.addPoi("invalid_name","Amsterdam");
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==1);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);



	}

	/**
	 * route.addPoi("valid poi","invalid wp"); AmsterDam  in route
	 */
	void testaddPoi_validPOIinvalidWp() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);

		route.addPoi("mitte","invalid Amsterdam");
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==0);




	}

	void testaddPoi_validPOIvalidWp() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("Amsterdam");
		route.addPoi("mitte","Amsterdam");
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==2);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);
		CPPUNIT_ASSERT(routeObjects[1]->getName()=="mitte");
		CPPUNIT_ASSERT(((CPOI*)routeObjects[1])->getType()=="RESTAURANT");

	}

	/**
	 * add duplicate POi
	 * @return
	 */
	void testaddPoi_validPOIDuplication() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("Amsterdam");
		route.addPoi("mitte","Amsterdam");
		route.addPoi("mitte","Amsterdam");
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		sizeAfterAdd=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAdd==2);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);
		CPPUNIT_ASSERT(routeObjects[1]->getName()=="mitte");
		CPPUNIT_ASSERT(((CPOI*)routeObjects[1])->getType()=="RESTAURANT");

	}
	static TestSuite* suite() {
		CppUnit::TestSuite* suite = new CppUnit::TestSuite("Addwp() testing");
		suite->addTest(new CppUnit::TestCaller<CaddPoiTest>
		("addPoiTest_DBnoInit", &CaddPoiTest::addPoiTest_DBnoInit));
		suite->addTest(new CppUnit::TestCaller<CaddPoiTest>
		("testaddPoi_invalidPOI", &CaddPoiTest::testaddPoi_invalidPOI));
		suite->addTest(new CppUnit::TestCaller<CaddPoiTest>
		("testaddPoi_invalidPOIvalidWp", &CaddPoiTest::testaddPoi_invalidPOIvalidWp1));
		suite->addTest(new CppUnit::TestCaller<CaddPoiTest>
		("testaddPoi_invalidPOIvalidWp2", &CaddPoiTest::testaddPoi_invalidPOIvalidWp2));
		suite->addTest(new CppUnit::TestCaller<CaddPoiTest>
		("testaddPoi_validPOIinvalidWp", &CaddPoiTest::testaddPoi_validPOIinvalidWp));
		suite->addTest(new CppUnit::TestCaller<CaddPoiTest>
		("testaddPoi_validPOIvalidWp", &CaddPoiTest::testaddPoi_validPOIvalidWp));
		suite->addTest(new CppUnit::TestCaller<CaddPoiTest>
		("testaddPoi_validPOIDuplication", &CaddPoiTest::testaddPoi_validPOIDuplication));
		return suite;
	}
};

#endif /* CADDPOI_H_ */
