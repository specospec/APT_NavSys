/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************
* Filename        :CNavigationSystem.h
* Author          :Vinu
* Description     : Implementation of Navigation system class
* 					 Functionality is implemented in public function run()
* 					 which sets up a route prints it and calculate distance to nearest POI
*
*
****************************************************************************/

#ifndef CNAVIGATIONSYSTEM_H
#define CNAVIGATIONSYSTEM_H
#include "CGPSSensor.h"
#include "CRoute.h"
#include "CPoiDatabase.h"
#include "CfileStorage.h"
#include "CJsonPersistence.h"
class CNavigationSystem {
private:
    void printRoute(); 					// PRivate member function to print the route
    void enterRoute();					// PRivate member function to create the route
    void printDistanceCurPosNextPoi();	// PRivate member function to acquire current location via GPS and print the distance to nearest POI

    /**
     * @link aggregationByValue 
     */
    CGPSSensor m_GPSSensor;				// To access the GPS

    /**
     * @link aggregationByValue 
     */
    CPoiDatabase m_PoiDatabase;			// Database for navigation

    CwpDatabase m_WpDataBase;

    /**
     * @link aggregationByValue 
     */
    CRoute m_route;						// To access the route functionality


public:

    CNavigationSystem();				// Default constructor
    void run();							// Function coordinating the Navigation functionality
};
/********************
**  CLASS END
*********************/
#endif /* CNAVIGATIONSYSTEM_H */
