/*
 * mytest.h
 *
 *  Created on: Jan 8, 2016
 *      Author: vinu
 */

#ifndef MYTEST_H_
#define MYTEST_H_
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include "CwayPoint.h"
#include "CRoute.h"
using namespace CppUnit;
class CtestaddWp: public TestFixture
{
public:
	CtestaddWp(){

	}

	CRoute route;

	CwayPoint testwp,Berlin,Amsterdam;
	CPOI mitte,Kinopolis;

	CPoiDatabase PoiDatabase;			// Database for navigation

	CwpDatabase WpDataBase;

	void setUp()
	{
		//Do the setup required before each testing
		testwp= CwayPoint("dummy",10,10);
		Berlin=CwayPoint("Berlin",52.5167,13.4);
		Amsterdam=CwayPoint("Amsterdam",52.3667,4.9000);
		mitte=CPOI(RESTAURANT,"mitte","Regular Place",49.87477, 8.656423);
		Kinopolis=CPOI(KINO,"Kinopolis","best german movies",49.8717537,9.6331035);

		WpDataBase.addWaypoint(Berlin);
		WpDataBase.addWaypoint(Amsterdam);
		WpDataBase.addWaypoint(testwp);

		PoiDatabase.addPoi(Kinopolis);
		PoiDatabase.addPoi(mitte);

	}
	/**
	 * cleanup operations after each testcase
	 */
	void tearDown()
	{
		// cleanup operations after each testcase
	}
	/**
	 * adding element before database init
	 */
	void testaddWp_noDBinit()
	{
		route.ClearRoute();
		route.ClearRoute();
		route.addWaypoint("test");
		route.addWaypoint("test");
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		int sizeAftAddWp=routeObjects.size();
		CPPUNIT_ASSERT(sizeAftAddWp==0);

	}
	/**
	 * adding invalid wp(not present in DB) after DB init
	 */
	void testaddWp_invalidWp() {
		route.ClearRoute();
		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("invalid_name");
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		int sizeAfterAddWp=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAddWp==0);

	}
	/**
	 * Adding a valid Wp
	 */
	void testaddWp_validWp() {

		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("Amsterdam");
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		int sizeAfterAddWp=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAddWp==1);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);
	}

	/**
	 * Adding a valid Wp back to back
	 */

	void testaddWp_validWpRepeat() {

		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("Amsterdam");
		route.addWaypoint("Amsterdam");// repeat
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		int sizeAfterAddWp=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAddWp==1);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);
	}
	/**
	 * adding differnt wp back to back
	 */
	void testaddWp_validWpDifferent() {

		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("Amsterdam");
		route.addWaypoint("Berlin");// repeat
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		int sizeAfterAddWp=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAddWp==2);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);
		CPPUNIT_ASSERT(routeObjects[1]->getName()=="Berlin");
		CPPUNIT_ASSERT(routeObjects[1]->getLatitude()==52.5167);
		CPPUNIT_ASSERT(routeObjects[1]->getLongitude()==13.4);

	}
	/**
	 * add Amsterdam,Berlin,Amsterdam
	 */
	void testaddWp_validWp121() {

		route.connectToPoiDB(&PoiDatabase);
		route.connectToWpDB(&WpDataBase);
		route.addWaypoint("Amsterdam");
		route.addWaypoint("Berlin");
		route.addWaypoint("Amsterdam");// repeat
		const vector<const CwayPoint*> routeObjects=	route.getRoute();
		int sizeAfterAddWp=routeObjects.size();

		CPPUNIT_ASSERT(sizeAfterAddWp==3);
		CPPUNIT_ASSERT(routeObjects[0]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[0]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[0]->getLongitude()==4.9000);

		CPPUNIT_ASSERT(routeObjects[1]->getName()=="Berlin");
		CPPUNIT_ASSERT(routeObjects[1]->getLatitude()==52.5167);
		CPPUNIT_ASSERT(routeObjects[1]->getLongitude()==13.4);

		CPPUNIT_ASSERT(routeObjects[2]->getName()=="Amsterdam");
		CPPUNIT_ASSERT(routeObjects[2]->getLatitude()==52.3667);
		CPPUNIT_ASSERT(routeObjects[2]->getLongitude()==4.9000);
	}


	static TestSuite* suite()
	{
		CppUnit::TestSuite* suite = new CppUnit::TestSuite("Addwp() testing");
		suite->addTest(new CppUnit::TestCaller<CtestaddWp>
		("testaddWp_invalidWp", &CtestaddWp::testaddWp_invalidWp));
		suite->addTest(new CppUnit::TestCaller<CtestaddWp>
		("testaddWp_noDBinit", &CtestaddWp::testaddWp_noDBinit));
		suite->addTest(new CppUnit::TestCaller<CtestaddWp>
		("testaddWp_validWp", &CtestaddWp::testaddWp_validWp));
		suite->addTest(new CppUnit::TestCaller<CtestaddWp>
		("testaddWp_validWpRepeat", &CtestaddWp::testaddWp_validWpRepeat));
		suite->addTest(new CppUnit::TestCaller<CtestaddWp>
		("testaddWp_validWpDifferent", &CtestaddWp::testaddWp_validWpDifferent));
		suite->addTest(new CppUnit::TestCaller<CtestaddWp>
		("testaddWp_validWp121", &CtestaddWp::testaddWp_validWp121));
		return suite;
	}
};

#endif /* MYTEST_H_ */
