
/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************/

// System Headers

#include <string>
#include <iostream>
#include <math.h>
using namespace std;


// Own Headers


// Implementation
#include "CwayPoint.h"

/**
 *constructor
 * @param name
 * @param latitude
 * @param logitude
 */
CwayPoint::CwayPoint(string name,double latitude, double logitude)
{
#ifdef DEBUGPRINTING
	extern int count_constructor;
	cout<<"Constructing CwayPoint: "<<count_constructor++<<"@"<<this<<endl;
#endif

	set(name,latitude,logitude); // Input data validation and member initialization

#ifdef SHOWADDRESS
	cout<<endl<<"constructing object at "<<this<<endl;
	cout<<"Printing Element properties"<<endl;
	cout<<this->m_name<<" @ " << &m_name <<" is of "<<sizeof(this->m_name)<<" Bytes"<<endl;
	cout<<this->m_latitude<<" @ " << &m_latitude <<" is of "<<sizeof(this->m_latitude)<<" Bytes"<<endl;
	cout<<this->m_longitude<<" @ " << &m_longitude <<" is of "<<sizeof(this->m_longitude)<<" Bytes"<<endl;
#endif
}

/**
 *
 */

CwayPoint::~CwayPoint(){

}
/**
 *getter for name
 * @return name of waypoint
 */
string CwayPoint::getName()const
{
	
	return this->m_name;

}
/**
 *getter for latitude
 * @return latitude of waypoint
 */
double CwayPoint::getLatitude()const{
	
	return m_latitude;
}
/**
 *getter for longitude
 * @return longitude of waypoint
 */
double CwayPoint::getLongitude()const{
	
	return m_longitude;
}

/**
 * Get the name, latitude and longitude
 * @param name		: return name of name of waypoint
 * @param latitude	: return latitude of name of waypoint
 * @param longitude	: return longitude of name of waypoint
 */
void CwayPoint::getAllDataByReferance(string &name, double &latitude, double  &longitude)const
{
	name=this->m_name;
	latitude=this->m_latitude;
	longitude=this->m_longitude;

	/**
	 * Call by reference works internally by using pointers or rather say address of variables being passed on to the function
	 */
#ifdef SHOWADDRESS
	cout<<"get all data by ref\n";
	cout<<name<<" @ name" << &name <<" is of "<<sizeof(name)<<" Bytes"<<endl;				// String always takes 4 bytes since it is a poiter
	cout<<latitude<<"lat @ " << &latitude <<" is of "<<sizeof(latitude)<<" Bytes" <<endl;	// takes 8 bytes
	cout<<longitude<<"lon @ " << &longitude <<" is of "<<sizeof(longitude)<<" Bytes"<<endl;	// takes 8 bytes
#endif
}

/**
 * Setter for name , latitude and longitude
 * @param name		: Name of Waypoint to be set
 * @param latitude	: latitude of Waypoint to be set
 * @param longitude	: longitude of Waypoint to be set
 */
void CwayPoint::set(string name, double latitude, double longitude)

{

#ifdef DEBUGPRINTING

	cout<<"setting CwayPoint:lat  "<<latitude<<" long "<<longitude<<endl;
#endif

	m_name=name;

	if(longitude>=-180 && longitude <=180) 	//   longitude valid values range check
	{
		m_longitude=longitude;
	}
	else 									// Print error message and set value to default.(0.0)
	{
		cout<<"Invalid longitude. Longitude <= |180|, Setting Longitude to 0.0"<<endl;
		m_longitude=0;
	}
	if (latitude>=-90 && latitude <=90)  	//   latitude valid values check
	{
		m_latitude=latitude;
	}
	else 									// Print error message and set value to default.(0.0)
	{
		cout<<"Invalid Latitude. Latitude <= |90|, Setting Latitude to 0.0"<<endl;
		m_latitude=0;
	}
}

/**
 *	 			calculate the distance between current waypoint and wp
 * @param wp	: The waypoint to which the distance to be calculated from this object
 * @return		:returns the distance between this and wp
 */
double CwayPoint::calculateDistance(const CwayPoint &wp)
{
	double latitude_1	=	this->m_latitude*TORADIAN,
		   longitude_1	=	this->m_longitude*TORADIAN,
		   latitude_2	=	wp.m_latitude*TORADIAN,
		   longitude_2  =	wp.m_longitude*TORADIAN; // for better readability and for Radian convertion
/*
 * Formula for distance calculation from two geographic coordinates
 */
	return  RADIUS_EARTH * acos(sin(latitude_1) * sin(latitude_2) +
			(cos( latitude_1) * cos(latitude_2) * cos(longitude_2 - longitude_1)));
}



/**
 *				prints the waypoint details
 * @param format : DEGREE for printing in degree format
 * 					MMSS for printing in minute second format
 */
void CwayPoint:: print(int format)
{
	int deg,mm;
	double ss;

	switch(format)
	{
	case DEGREE:
	{

		/*
		 *Printing in degrees
		 */
#ifdef DEBUGPRINTING
		cout<<"\n Printing in degrees \n"<<endl;
#endif
		cout<<"Location: "<<m_name<<", Latitude : "<<m_latitude<<" degree, Logitude : " <<m_longitude<<" degree"<<endl;
		break;
	}
	case MMSS:
	{
		/*
		 *Printing in degree minutes seconds format
		 */
#ifdef DEBUGPRINTING
		cout<<endl<<"Printing in degree minutes seconds format "<<endl;
#endif



		transformLatitude2degmmss(deg, mm,ss);	// call by reference to get degree, minute, second values
		cout<<"Location  :"<<m_name<<endl <<"Latitude: " <<deg <<" degree, "<<mm<<" minute, "<<ss<<" second "<<endl;

		transformLongitude2degmmss(deg, mm,ss);  // call by reference to get degree, minute, second values
		cout<<"Longitude: "<<deg <<" degree, "<<mm<<" minute, "<<ss<<" second "<<endl;
		break;
	}
	default : cout <<"invalid printing format";
	};
}

/**
 * 				Returns the converted parameters via Call be reference
 * @param deg	: Degree
 * @param mm	: minute
 * @param ss	: second
 */
void CwayPoint::transformLongitude2degmmss(int &deg, int &mm, double &ss)const
{
	/*
	 * 1 degree = 60 minutes
	 * 1 minute = 60 seconds
	 */
	deg = (int)m_longitude; // type casting always rounds to lowest integer value
	mm  = (m_longitude-deg)*60;
	ss  = ((m_longitude-deg)*60-mm)*60;

}

/**
 * Returns the converted parameters via Call be reference
 * @param deg	: Degree
 * @param mm	: minute
 * @param ss	: second
 */
void CwayPoint:: transformLatitude2degmmss(int & deg, int &mm, double &ss)const
{
	/*
	 * 1 degree = 60 minutes
	 * 1 minute = 60 seconds
	 */
	deg = (int)m_latitude; // type casting always rounds to lowest integer value
	mm  = (m_latitude-deg)*60;
	ss  = ((m_latitude-deg)*60-mm)*60;

}
/**
 * this is  virtual function. CwayPoint clone() returns a pointer to CwayPoint type
 * function creates a copy of the current object and returns the pointer to it
 * @return : pointer to current object
 */
CwayPoint* CwayPoint::clone()const
{
#ifdef DEBUGPRINTING
	cout<<"CwayPoint clone"<<endl;
#endif

	   return new CwayPoint(*this);	// creates a copy of the current object and returns the pointer to it
}

/**
 * Function prints the waypoint object to output stream
 * @param output	: out put stream
 * @param wp		: waypoint object to be printed
 * @return			: retuns the updated stream object
 */
ostream& operator<<(ostream &output,const CwayPoint &wp){

		int deg,mm;
		double ss;
	output<<wp.getName()<<" @ (";
	wp.transformLatitude2degmmss(deg, mm,ss);
	output<<deg<<"� "<<mm<<"' "<<ss<<"\" "<<",";
	wp.transformLongitude2degmmss(deg, mm,ss);
	output<<deg<<"� "<<mm<<"' "<<ss<<"\" "<<")"<<endl;
	return output;
}





