/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************
* Filename        :CfileStorage.h.h
* Author          :Vinu
* Description     :Implements persistant file handling capabiities
* 					interfaces for reading writing and setting the file name
*
*
****************************************************************************/
#ifndef CFILESTORAGE_H
#define CFILESTORAGE_H

#include "CPersistentStorage.h"
#define INVALID_POI_TYPE ((t_poi)255)
#define NUM_ADDPOI_PARAMS (5)
#define NUM_ADDWP_PARAMS (3)
enum t_parseError {NOERROR,TYPE_INVALID,NAME_INVALID,DESCRIPTION_INVALID,LATITUDE_INVALID,LONGITUDE_INVALID,PARAMETER_COUNT_INVALID};
extern string  pois[];
t_poi t_poiToString(string type);
class CfileStorage : public CPersistentStorage
{private:
	string m_fileName;
	bool readDataPOI ( CPoiDatabase& poiDb);
	bool readDataWayPoint (CwpDatabase& waypointDb);
public:

	 void setMediaName(string name);
	 bool writeData (const CwpDatabase& waypointDb,const CPoiDatabase& poiDb);
	 bool readData (CwpDatabase& waypointDb, CPoiDatabase& poiDb,MergeMode mode);
	 void  printWaypointDatabaseToStream(map<string,CwayPoint> data,ofstream& char_traits) const;
	 void  printPOIDatabaseToStream(map<string,CPOI> data,ofstream& char_traits) const;
~CfileStorage();
};

#endif /* CFILESTORAGE_H */

