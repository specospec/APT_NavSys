/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************/

// System Headers

#include <string>

using namespace std;


// Own Headers


// Implementation
#include "CPOI.h"
/*
 * array of POI type strings.Used to print the Type.
 */
string pois[]={"DEFAULT","RESTAURANT","KINO","GAS","ATM","SIGHTSEEING","PARK","CITYCENTER"};

/*
 * Nested constructor to initialize all the member variables.
 */
CPOI::CPOI(t_poi type, string name, string description,double latitude, double logitude):
			CwayPoint(name,latitude,logitude)
{
#ifdef DEBUGPRINTING
	extern int count_constructor;
	cout<<"Constructing CPOI: "<<count_constructor++<<"@"<<this<<endl;
#endif

	set(type,description);
}
/**
 * Prints the POI object properties
 */
void CPOI::print(int format)
{

	/*
	 * Print member variables
	*/
#ifdef DEBUGPRINTING

	cout<<"CPOI::print() Printing Point of Interest details"<<endl;
#endif

	CwayPoint::print(format);	// calling CwayPoint print method to print name , longitude and latitude.
	cout<<"of Type "<<pois[m_type]<<":"<<m_description<<endl;

}
/**
 *set member variables
 * @param type			: Type of POI
 * @param description	: Description of POI
 */
void CPOI::set(t_poi type,string description)
{
	m_type=type;
	m_description=description;
}


/**
 *get All Data By Reference: Returns all the member elements by reference
 * @param name			: Name of POI
 * @param latitude		: latitude of POI
 * @param logitude		: logitude of POI
 * @param type			: type of POI
 * @param description	: description of POI
 */
void CPOI::getAllDataByReference(string &name,double &latitude, double &logitude, t_poi& type, string& description)const
{
	getAllDataByReferance(name,latitude,logitude);
	type=m_type;
	description=m_description;
}
/**
 * Getter function for m_type : POI type
 * @return : returns the type as a string
 */
string CPOI::getType()const 	// const since function is not changing the data members
{
	return pois[m_type]; 	// string corrresponding to POI type is obtained by using t_poi as an index to array storing POI types in the same order as they are defined in Enum
}
/**
 * Getter functionm_description
 * @return m_description
 */
string CPOI::getDescription()const
{
	return m_description;
}

/**
 * CPOI clone() return pointer to CPOI type but the return type is CwayPoint*
 * function creates a copy of the current object and returns the pointer to it
 * @return : pointer to current object
 */
CwayPoint* CPOI:: clone()const
{

#ifdef DEBUGPRINTING
	cout<<"CPOI clone"<<endl;
#endif

return (new CPOI (*this)); //returns pointer to current object

}

/**
 * overloaded indirection operator for priting data to output stream
 * @param output	: stream name
 * @param Poi		: Poi to be printed
 * @return			: returns stream object
 */
ostream &operator<<(ostream &output,const CPOI &Poi)
{
	output<<(CwayPoint)Poi;
	output<<"is a : "<<pois[Poi.m_type]<<" - \" "<<Poi.m_description<<" \""<<"\n";
	return output;
}
