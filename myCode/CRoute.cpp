/***************************************************************************
 *============= Copyright by Darmstadt University of Applied Sciences =======
 ****************************************************************************/

// System Headers

#include <string>

using namespace std;


// Own Headers


// Implementation
#include "CRoute.h"
#include "CPOI.h"
#include "CwayPoint.h"

/**
 * constructor default
 */
CRoute::CRoute()
{
	/*
	 * Constructor intializing the member elements and allocate memory for pointers.
	 */
#ifdef DEBUGPRINTING
	extern int count_constructor;
	cout<<"constructing CRoute: parm constructor: "<<count_constructor++<<"@"<<this<<endl;
#endif

	// initilizing both databases to null
	m_pPoiDatabase = NULL ;				// Not dynamically allocating since it will be initialized by connectToPoiDB()
	m_pWpDatabase=NULL;


}


/**
 *copy constructor
 * @param origin	Copy object
 */
CRoute::CRoute(const CRoute &origin)
{

#ifdef DEBUGPRINTING
	cout<<"copying CRoute: copy constructor"<<endl;
#endif
	*this=origin;  						// copy constructor calls the overloaded = operator which does the copying of origin to current object

}

/**
 * Destructor
 * deleting the memory allocated to store the pointers in vector
 *
 */
CRoute::~CRoute()
{
#ifdef DEBUGPRINTING
	cout<<" Destructing Route object at"	<<this<<endl;
#endif
	vector<CwayPoint*>::iterator itrWpPoi;
	for (itrWpPoi=m_pVectorWpPoi.begin();itrWpPoi!=m_pVectorWpPoi.end();itrWpPoi++)
	{
		delete *itrWpPoi; 				// deallocate memory
	}

	m_pVectorWpPoi.clear(); 			// clear the vector
}

/**
 *Function to connect to and external database
 * @param pPoiDB	: Pointer to CPoiDatabase object to be connected to Croute object
 */
void CRoute::connectToPoiDB(CPoiDatabase* pPoiDB)
{
#ifdef DEBUGPRINTING
	cout<<"Connecting to DB\n";
#endif

	if (pPoiDB != NULL) 				// check the pointer is initialized
	{
		m_pPoiDatabase=pPoiDB; 			// store the pointer to database in member element.
	}
	else
	{
		m_pPoiDatabase=NULL;
	}

}

/**
 *Function to connect to and external database
 * @param pWpDB	: Pointer to CWpDatabase object to be connected to Croute object
 */
void CRoute::connectToWpDB(CwpDatabase* pWpDB)
{

#ifdef DEBUGPRINTING
	cout<<"Connecting to DB\n";
#endif

	if (pWpDB != NULL) 				// check the pointer is initialized
	{
		m_pWpDatabase=pWpDB; 		// store the pointer to database in member element.
	}
	else
	{
		m_pWpDatabase=NULL;
	}
}

/**
 *Adds a waypoint to route
 * @param wp	: waypoint object to be added to route
 */
void CRoute::addWaypoint(string name)
{
	vector<CwayPoint*>::reverse_iterator revItrWpPoi;
	/*
	 * Add a new wp to current route
	 * waypoints are added to the dynamically created array
	 */
#ifdef DEBUGPRINTING
	cout<<"\n adding Way point \n";
#endif


	if ( m_pWpDatabase!= NULL) 									// Check Wpdatabase is connectead to route
	{

		if( m_pWpDatabase->getPointerToWaypoint(name) == NULL ) // check waypoint name exists in wpdatabase
		{
			//cout<<"ERROR: CRoute::addWaypoint() : Waypoint : "<<name<<" not found in database"<<endl;
		}
		else 													// if wp name is found add it to route
		{
			CwayPoint* tempWp=new CwayPoint ; 					// create memory for the waypoint object
			*tempWp = * m_pWpDatabase->getPointerToWaypoint(name); // save the waypoint to the newly created memory

			if( checkDuplicateWp(name,revItrWpPoi)== false) 				// check if waypoint: name is not present in route
			{
				m_pVectorWpPoi.push_back(tempWp); 				//Waypoint : name added to Route
			}
			else 												// else still add waypoint to route but ...
			{
				if( m_pVectorWpPoi.back()->getName() != tempWp-> getName()|| (dynamic_cast<CPOI*>(m_pVectorWpPoi.back())!=NULL))// add new waypoint if the last element name does not match new wp name or simply the last element is a POI type
				{
					m_pVectorWpPoi.push_back(tempWp);

				}
				else//do not add if the new waypoint is the last waypoint in the route
				{
					//cout<<"INFO : CRoute::addWaypoint() : Waypoint : "<<name<<" already present in Route"<<endl;

				}
			}
		}

	}
	else
	{
		//cout<<"ERROR: CRoute::addWaypoint() : Point of interest or Waypoint Database not initialized"<<endl;

	}

}


/**
 * Adds POI to current route.
 *
 * @param namePoi 	: Name of POI to be added
 * @param afterWP	: New POI will be added after wp "afterWP", if afterwp not present namePoi added to end of route
 */
void  CRoute::addPoi(string namePoi,string afterWP)
{
	vector<CwayPoint*>::reverse_iterator revItrWpPoi;
#ifdef DEBUGPRINTING
	cout<<"adding POI"<<endl;
#endif


	if (m_pPoiDatabase!=NULL && m_pWpDatabase!=NULL) 					// Check database initialization
	{

		if( (m_pPoiDatabase->getPointerToPoi(namePoi)) == NULL ) 		// namePoi not present in POI  database
		{
			//cout<<"ERROR: CRoute::addPoi("<<namePoi<<","<<afterWP<<") - Point of Interest \""<<namePoi<<"\" not found in database"<<endl;
		}
		else															//add namePoi  to route
		{
			if (checkDuplicatePoi(namePoi))
			{
				//namePoi can not be added to route since it is present already
				//cout<<"ERROR: CRoute::addPoi("<<namePoi<<","<<afterWP<<") - Point of interest : "<<namePoi<<" already present in Route"<<endl;

			}
			else
			{
				CPOI * tempPoi=new CPOI;								// create memory for the CPOI object
				*tempPoi=*m_pPoiDatabase->getPointerToPoi(namePoi); 	// store the poi to the newly created memory

				//				if(NULL == m_pWpDatabase->getPointerToWaypoint(afterWP) )// afterWp not present in database also not present in route
				//				{
				//					cout<<"INFO : CRoute::addPoi("<<namePoi<<","<<afterWP<<") - Waypoint \""<<afterWP<<"\" not found in WP database.Adding Point of Interest \""<<namePoi<<"\" to the end of current route"<<endl;
				//					m_pVectorWpPoi.push_back(tempPoi);
				//
				//				}
				//				else
				if (checkDuplicateWp(afterWP,revItrWpPoi))						// check if afterWP is present in route this will update the "revItrWpPoi"
				{
					// insert the pointer to namePoi in route vector
					m_pVectorWpPoi.insert(revItrWpPoi.base(),tempPoi);  //insert()  works only with normal iterators so reverse iterator is converted to regular iterator using the base() function

				}
				//				else 													// afterWP not present in route, adding route to end of vector
				//				{
				//					//cout<<"INFO : CRoute::addPoi("<<namePoi<<","<<afterWP<<") - Waypoint \""<<afterWP<<"\" not found in route.Adding Point of Interest \""<<namePoi<<"\" to the end of current route"<<endl;
				//					m_pVectorWpPoi.push_back(tempPoi);
				//
				//				}
			}
		}
	}
	else
	{
		//cout<<"ERROR: addPoi("<<namePoi<<","<<afterWP<<") Point of interest or Waypoint Database not initialized"<<endl;
	}


}

/**
 *Function getDistanceNextPoi returns the  distance from the waypoint wp to nearest POI in the route..
 * and stores the nearest POI object in poi
 * @param wp	: Current location
 * @param poi	: Nearest POI will  be updated to this pointer
 * @return		: Distance to nearest POI
 */
double CRoute::getDistanceNextPoi( CwayPoint const &wp,CPOI * poi)
{
#ifdef DEBUGPRINTING
	int i;
#endif
	double distance;  																// Local array to store the distance to each POI
	double min=numeric_limits<double>::max();										// min initialized with MAX value of double.
	vector<CwayPoint*>::iterator itrWpPoi;
	if (m_pVectorWpPoi.empty())  													// check if POIs are present
	{
		//cout << " CRoute::getDistanceNextPoi(): No POIs in route, can not calculate distance "<<endl;
		return min;																	// For error printing by calling function
	}
	else
	{

		for (itrWpPoi=m_pVectorWpPoi.begin();itrWpPoi!=m_pVectorWpPoi.end();itrWpPoi++)
		{
			distance = (*itrWpPoi)->calculateDistance(wp);							// calculate the distance using CPOI member function which is inherited from CwayPoint

			if ( (distance < min) && (dynamic_cast<CPOI*>(*itrWpPoi)!=NULL)	)		// minimum is updated only for POIs
			{																		// each distance is checked for minimum
				min=distance;														// if a minimum distance is found update minimum
				*poi=*(dynamic_cast<CPOI*>(*itrWpPoi));								// update poi to the minimum distance CPOI object

			}

#ifdef DEBUGPRINTING
			cout<<wp.getLatitude()<<endl;
			//cout<<(dynamic_cast<CPOI*>(*itrWpPoi))<<endl;
			cout<<"Distance "<<i<<" : "<<distance<<endl;
			cout<<"minimum "<<i<<" : "<<min<<endl;
			i++;
#endif
		}

		return min;												// returns the minimum value
	}

	return min; 												// returns <double>::max() incase or error
}

/**
 * Prints Waypoints and POIs in the route
 */
void CRoute::print()
{
	int i=1;													// local var for count
	vector<CwayPoint*>::iterator itrWpPoi;
	cout<<"\nPrinting Route \nThere are "<<m_pVectorWpPoi.size()<<" WP/POI in route"<<endl;
	if(m_pVectorWpPoi.size()!= 0)
	{
		for (itrWpPoi=m_pVectorWpPoi.begin();itrWpPoi!=m_pVectorWpPoi.end();itrWpPoi++)
		{
			cout<<i++<<". ";									//print count
			// Printing using virtual function print()
			//			(*itrWpPoi)->print(2); 					// this always calls the correct print function since Cwaypoint print is virtual.
			// cout <<*(*itrWpPoi)<<endl 						// this prints everything as cwaypoint because pointer indirection returns a cwaypoint objects since we store cwaypoint pointers in vector
			// Printing using dynamic casting and overloaded << operator
			if(dynamic_cast<CPOI*>(*itrWpPoi)!=NULL) 			// check pointer is of type CPOI
			{
				cout<<*(dynamic_cast<CPOI*>(*itrWpPoi))<<endl; // print the CPOi object using the overloaded << operator
			}
			else if(dynamic_cast<CwayPoint*>(*itrWpPoi)!=NULL) // else it should be waypoint but anyway checking
			{
				cout<<*(dynamic_cast<CwayPoint*>(*itrWpPoi))<<endl; //  print the cwaypoint object using the overloaded << operator
			}
		}
	}

}




/**
 *Checks the route vector for wp object with name nameWp and returns the search of search and updates the object iterator
 * @param nameWp : Name of the wp to be checked
 * @return 		  : true if wp  present in route
 * 					false in case wp not found
 * 					revItrWpPoi  is updated if the nameWp is found
 */
bool CRoute::checkDuplicateWp(string nameWp,vector<CwayPoint*>::reverse_iterator &revItrWpPoi){

	bool flag=false;												// initial status

	//CwayPoint*	poToWp=m_pWpDatabase->getPointerToWaypoint(nameWp);	// load pointer to nameWp
//cout<<"pointer"<<poToWp;
	if(!m_pVectorWpPoi.empty())										// check vector is not empty
	{
		for (revItrWpPoi=m_pVectorWpPoi.rbegin();revItrWpPoi!=m_pVectorWpPoi.rend();revItrWpPoi++) // reverse iterator is used to search the vector from back to front
		{//cout<<"getname"<<(*revItrWpPoi)->getName()<<endl;
			if( ( (*revItrWpPoi)->getName() == nameWp )&& (dynamic_cast<CPOI*>(*revItrWpPoi)==NULL) )// check poToWp against each pointer in vector
			{

#ifdef DEBUGPRINTING
				cout<<	"Cast "<<(dynamic_cast<CPOI*>(*revItrWpPoi))<<endl;
				cout<<"data "<<(*revItrWpPoi)->getName()<<endl;
				cout<<" wp duplicate found  : "<<nameWp<<endl;
#endif
				flag=true;												// update status
				break;												// break after duplicate is found
			}
			else
			{
				flag=false;											// else status false
			}

		}

	}

	return flag;													//  return default status false
}
/**
 *Checks the route vector for Poi object with name namePoi
 * @param namePoi : Name of the POI to be checked
 * @return 		  : true if poi  present in route
 * 					false in case poi not found
 * 					also class iterator itrWpPoi is updated
 */
bool CRoute::checkDuplicatePoi(string namePoi){

	bool flag=false;												// initial status
	//CPOI*	poToPOI=m_pPoiDatabase->getPointerToPoi(namePoi);		// load pointer to namePoi
	vector<CwayPoint*>::iterator itrWpPoi;

	if(!m_pVectorWpPoi.empty())										// check vector is not empty
	{
		for (itrWpPoi=m_pVectorWpPoi.begin();itrWpPoi!=m_pVectorWpPoi.end();itrWpPoi++) // iterate through the vector
		{
			if((*itrWpPoi)->getName() == namePoi&& (dynamic_cast<CPOI*>(*itrWpPoi)!=NULL))		// check namePoi against each POI in vector
			{
#ifdef DEBUGPRINTING
				cout<<"poi duplicate found  : "<<namePoi<<endl;
#endif
				flag=true;											// set status and break
				break;
			}
			else flag=false;										// reset flag

		}

	}
	return flag;													//return default
}
/**
 * function which takes a string representing the name of a POI or waypoint,
 * searches a corresponding POI and/or waypoint in the databases and
 * adds them to the end of the route.
 *
 * If both a waypoint and a POI are found, the waypoint will be added before the POI.
 * @param WpOrPoi	: string representing the name of a POI or waypoint,
 */
void CRoute::operator +=(string WpOrPoi)
						{

#ifdef DEBUGPRINTING
	cout<<"\n calling operator+ CRoute \n";
#endif
	vector<CwayPoint*>::reverse_iterator revItrWpPoi;
	addWaypoint(WpOrPoi);		// adds the waypoint WpOrPoi to route
	if (m_pPoiDatabase!=NULL && m_pWpDatabase!=NULL)
	{
		if (checkDuplicateWp(WpOrPoi,revItrWpPoi))
		{
			addPoi(WpOrPoi,WpOrPoi);	// adds the Poi "WpOrPoi" to route
			// If both a waypoint and a POI are found,

		}
		else if((m_pPoiDatabase->getPointerToPoi(WpOrPoi)) != NULL)
		{

			CPOI * tempPoi=new CPOI;								// create memory for the CPOI object
			*tempPoi=*m_pPoiDatabase->getPointerToPoi(WpOrPoi); 	// store the poi to the newly created memory
			m_pVectorWpPoi.push_back(tempPoi);
		}
	}
						}

/**
 * operator overloaded operator+ adds 2 routes by concatenating the content,
 *  if they are connected to the same waypoint and POI database.
 *   If not, an error message is shown and an empty route is returned
 * @param route2	: second route to be added, first route will be the object itselt
 * @return			: result route object
 */
CRoute CRoute::operator +(const CRoute &route2)
{
#ifdef DEBUGPRINTING
	cout<<"\n calling operator+ CRoute \n";
#endif
	vector<CwayPoint*>::const_iterator constitrWpPoi;
	CRoute temp;																					// Temporary route object
	if( this->m_pPoiDatabase==route2.m_pPoiDatabase && this->m_pWpDatabase==route2.m_pWpDatabase) 	// checking if both routes are connected to the same database
	{
		temp.connectToPoiDB(m_pPoiDatabase);														// initialize temp route
		temp.connectToWpDB(m_pWpDatabase);															// initialize temp route
		for (constitrWpPoi=m_pVectorWpPoi.begin();constitrWpPoi!=m_pVectorWpPoi.end();constitrWpPoi++)
		{
			temp.m_pVectorWpPoi.push_back((*constitrWpPoi)->clone());								// copy route 1 to temp
		}
		for (constitrWpPoi=route2.m_pVectorWpPoi.begin();constitrWpPoi!=route2.m_pVectorWpPoi.end();constitrWpPoi++)
		{
			temp.m_pVectorWpPoi.push_back((*constitrWpPoi)->clone());								// copy route 2 to temp
		}
#ifdef DEBUGPRINTING
		cout<<"\n printing temp  CRoute::operator +( \n";
		temp.print();
#endif
		return temp;
	}
	else
	{
		//cout<<"CRoute::operator + -- Database mismatch , returning an empty route \n";
		return CRoute(); // return an empty route
	}


}

void CRoute::ClearRoute()
{
	vector<CwayPoint*>::iterator itrWpPoi;
	for (itrWpPoi=m_pVectorWpPoi.begin();itrWpPoi!=m_pVectorWpPoi.end();itrWpPoi++)
	{
		delete *itrWpPoi; 				// deallocate memory
	}

	m_pVectorWpPoi.clear(); 			// clear the vector
}

void CRoute::operator=(const CRoute &origin)
{
#ifdef DEBUGPRINTING
	cout<<"\n calling operator= CRoute \n";
#endif
	vector<CwayPoint*>::const_iterator constitrWpPoi;
	for (constitrWpPoi=origin.m_pVectorWpPoi.begin();constitrWpPoi!=origin.m_pVectorWpPoi.end();constitrWpPoi++)
	{

		/*
		 *  the clone function creates the copy of the object pointed by each pointer in the vector,
		 *  since the clone method is virtual, the correct object copy is made depending on the type of pointer , Cwaypoint* or CPOI*
		 */
		m_pVectorWpPoi.push_back((*constitrWpPoi)->clone());

	}

	(this->m_pWpDatabase)=(origin.m_pWpDatabase); // copying the pointers to database.
	(this->m_pPoiDatabase)=(origin.m_pPoiDatabase); // copying the pointers to database.
}

const vector<const CwayPoint*> CRoute::getRoute()
{

	vector<const CwayPoint*> temp;
	vector<CwayPoint*>::const_iterator constitrWpPoi;
	for (constitrWpPoi=m_pVectorWpPoi.begin();constitrWpPoi!=m_pVectorWpPoi.end();constitrWpPoi++)
	{
		temp.push_back((*constitrWpPoi));								// copy route 2 to temp
	}
	return temp;

}
