/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************
* Filename        :Croute.h
* Author          :Vinu
* Description     :Class provides interfaces for
* 					Connecting to database.
* 					Adding POIs to the Route.
* 					Adding waypoints to the route.
* 					printing the added waypoints and POIs.
* 					Calculate the minimum distance to added POIs
*
*
****************************************************************************/

#ifndef CROUTE_H
#define CROUTE_H
#include "CPOI.h"
#include "CPoiDatabase.h"
#include "CwpDatabase.h"
//#include "CPersistentStorage.h"
extern string pois[]; // import decleration of POI type string.

class CwayPoint;

/**
 *
 */
class CRoute {
private:
    /**
     * @link aggregation 
     */
    vector<CwayPoint*> m_pVectorWpPoi;    // pointer to array storing the added waypoints in current Route
   // vector<CwayPoint*>::iterator itrWpPoi; // iterator to the vector used in member functions to traverse the vector
  //  vector<CwayPoint*>::reverse_iterator revItrWpPoi;
    //vector<CwayPoint*>::const_iterator constitrWpPoi;
   CwpDatabase * m_pWpDatabase;
    CPoiDatabase * m_pPoiDatabase;	// Pointer to database containing the POIS
    bool checkDuplicateWp(string nameWp,vector<CwayPoint*>::reverse_iterator &revItrWpPoi );
    bool checkDuplicatePoi(string namePoi);

public:

    CRoute();													// Default constructor

    CRoute(const CRoute &origin);								// Copy constructor

    ~CRoute();													// Destructor

    void connectToPoiDB(CPoiDatabase* pPoiDB);					// Connect to an eternal db
    const vector<const CwayPoint*> getRoute();
    void connectToWpDB(CwpDatabase* pWpDB);
    void addWaypoint(string);						// add a new waypoint to route

    void addPoi(string namePoi,string afterWP);								// add a poi from the db

    double getDistanceNextPoi(CwayPoint const  &wp,CPOI * poi);	// Calculate the distance to nearest POI
    void ClearRoute();
     void print();												// Prints all the waypoints and POI in the route
     void operator+=(string WpOrPoi);
     CRoute operator+(const CRoute &route2);
 	void operator=(const CRoute &route2);
};
/********************
**  CLASS END
*********************/
#endif /* CROUTE_H */
