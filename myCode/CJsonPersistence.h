/*
 * CJsonPersistence.h
 *
 *  Created on: Jan 3, 2016
 *      Author: vinu
 */

#ifndef CJSONPERSISTENCE_H_
#define CJSONPERSISTENCE_H_
#include "CPersistentStorage.h"
#include "CtemplateDatabase.h"
#include "CJsonScanner.h"
#include "CfileStorage.h"
namespace APT
{

class CJsonPersistence :public CPersistentStorage
{private:
	string m_fileName;
	template <typename T1>  void printDatabaseToStream(ofstream& char_traits, map<string,T1 > poimap);

enum parserStates{IDLE,DATA_ERROR,WAIT_SEPERATOR,WAIT_ARRAY,SAVE_OBJECT,WAIT_ATTRIBUTE,WAIT_DBNAME,WAIT_OBJECT,WAIT_ATRVALUE};
enum parserDBSubstate{NONE,WAYPOINTSTART,POISTART};
enum AttributeCS{RESET_CS=1,NAME_CS=2,LATI_CS=3,LONG_CS=5,TYPE_CS=7,DESC_CS=11,WAYPOINT_CS=30,POI_CS=2310}; // WAYPOINT_CS=2*3*7 ,POI_CS=2*3*5*7*11
public:
	 void setMediaName(string name);
	 bool writeData (const CwpDatabase & waypointDb,const CPoiDatabase &  poiDb);
	 bool readData (CwpDatabase & waypointDb, CPoiDatabase & poiDb,CPersistentStorage::MergeMode mode);

	CJsonPersistence();
	virtual ~CJsonPersistence();
};

} /* namespace APT */

#endif /* CJSONPERSISTENCE_H_ */
