/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************/

// System Headers

#include <string>
#include <iostream>

using namespace std;


// Own Headers


// Implementation
#include "CPoiDatabase.h"
#include "CPOI.h"

CPoiDatabase::CPoiDatabase()

{

#ifdef DEBUGPRINTING
	extern int count_constructor;
	cout<<"Constructing CPoiDatabase: "<<count_constructor++<<"@"<<this<<endl;
#endif


}

/**
 * function adds poi to database
 * @param poi	: POI object to be added to database
 *
 */
void CPoiDatabase::addPoi(const CPOI  &poi)
{

addLocation(poi);
//
//	if (m_mapPOI.count( poi.getName() ))
//	{
//
//		cout<<"CPoiDatabase::addPoi() POI "<<poi.getName()<<" already present in Database "<<endl;
//
//	}
//	else
//	{
//		m_mapPOI[poi.getName()]	=poi;										   // element added to DB. Counter incremented
//
//#ifdef DEBUGPRINTING
//		cout<<"CPoiDatabase::addPoi()  adding POI  "<<poi.getName()<<endl;
//#endif
//	}

}

/**
 *Search for a POI with name and return its address
 * @param name : name of POI to be searched in the current route
 * @return		: Returns the pointer to CPOI object if it is found
 * 				else returns null pointer
 */
CPOI* CPoiDatabase::getPointerToPoi(string name){
//
//	if(!m_mapPOI.empty())  					// check if map is empty
//	{
//		if(m_mapPOI.count( name )) 			// check if POI is present in the database
//			{
//				return &(m_mapPOI[name]); 	// if present return the address of POI object
//
//		}
//	}
	return getPointerToLocation(name); 							// no elemets in map or element not found
}
/*
 * Clear the database
 */
void CPoiDatabase::clear()
{
	mapClear();
	//m_mapPOI.clear();
}

/**
 * function to print Database in to an output file stream with
 * @param char_traits :  output stream
 */
//void CPoiDatabase::printPOIDatabaseToStream(ofstream& char_traits) const
//{
//
//
//	map<string,CPOI> ::const_iterator itr_map; // a constant iterator is required since const functions are called through the iterator
//	for (itr_map=m_mapPOI.begin();itr_map != m_mapPOI.end();itr_map++) 		// traversing the map using iterator
//	{
//		char_traits<<(itr_map->second).getType()<<";" 		// get each  properties using CPOI getter functions with returns string
//				<<(itr_map->second).getName()<<";"
//				<<(itr_map->second).getDescription()<<";"
//				<<(itr_map->second).getLatitude()<<";"
//				<<(itr_map->second).getLongitude()<<endl;
//		//char_traits <<
//
//	}
//
//}

map<string, CPOI> CPoiDatabase::getDatabase(void)const
{
	return getDatabaseMap();
}
