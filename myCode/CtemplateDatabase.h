/*
 * CtemplateDatabase.h
 *
 *  Created on: Jan 3, 2016
 *      Author: vinu
 */

#ifndef CTEMPLATEDATABASE_H_
#define CTEMPLATEDATABASE_H_
#include "CwayPoint.h"
namespace APT
{
template <class KEYTYPE,class VALUETYPE>
class CtemplateDatabase
{
private:
	map<KEYTYPE,VALUETYPE> m_mapLocation;
protected:

		void addLocation(const VALUETYPE &loc);
		VALUETYPE* getPointerToLocation(KEYTYPE);
		map<KEYTYPE,VALUETYPE> getDatabaseMap(void)const;
		void mapClear();

	
};



template <class KEYTYPE,class VALUETYPE>
 void APT::CtemplateDatabase<KEYTYPE,VALUETYPE>::addLocation(const VALUETYPE& loc)
{

	if (m_mapLocation.count( loc.getName() ))
	{
		//cout<<"CtemplateDatabase<VALUETYPE>::addLocation "<<loc.getName()<<" already present in  Database "<<endl;
	}

	else
	{
		//cout<<"adding Waypoint  "<<loc.getName()<<" to  database"<<endl;
		m_mapLocation[loc.getName()]	=loc;										   // element added to DB. Counter incremented
	}
}

template <class KEYTYPE,class VALUETYPE>
 VALUETYPE* APT::CtemplateDatabase<KEYTYPE,VALUETYPE>::getPointerToLocation(KEYTYPE allocator)
{
	if(!m_mapLocation.empty())
		{
			if(m_mapLocation.count( allocator ))
			{
				return &(m_mapLocation[allocator]);

			}
		}
		return NULL; // no elements in map or element not found
}

template <class KEYTYPE,class VALUETYPE>
 map<KEYTYPE, VALUETYPE> APT::CtemplateDatabase<KEYTYPE,VALUETYPE>::getDatabaseMap(void) const
{
	return m_mapLocation;
}


template <class KEYTYPE,class VALUETYPE>
 void APT::CtemplateDatabase<KEYTYPE,VALUETYPE>::mapClear()
{
	m_mapLocation.clear()	;
}
} /* namespace APT */
#endif /* CTEMPLATEDATABASE_H_ */
