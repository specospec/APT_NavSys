/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************/

// System Headers

#include <string>



// Own Headers


// Implementation
#include "CNavigationSystem.h"
using namespace std;
using namespace APT;
CNavigationSystem::CNavigationSystem()
{

#ifdef DEBUGPRINTING
	extern int count_constructor;
	cout<<"constructing CNavigationSystem: "<<count_constructor++<<"@"<<this<<endl;
#endif


}
/**
 * Function gets the current location via GPS functionality
 *  finds the nearest POI and prints distance to it.
 *
 */
void CNavigationSystem::printDistanceCurPosNextPoi()

{
	CwayPoint currentPos=m_GPSSensor.getCurrentPostion(); 			// invoke gps and get the current position
	CPOI  *nearestPOI =new CPOI ;									// To store the Nearest POI object via call by reference
	double distanceNear;											// local storage for distance
	distanceNear=m_route.getDistanceNextPoi(currentPos,nearestPOI);	// get the distance to nearest POI by calling the Croute member function
	if (distanceNear== numeric_limits<double>::max())											// Print error message if no POIs in route
	{
		cout<<"Error: Can not calculate distance, No POIs present in the route "<<endl;
	}
	else
	{
	cout<<"Nearest POI is "<<distanceNear<<" km away"<<endl;		// Print the distance  calculated
	nearestPOI->print(2);
	}// Print the nearest POI details
delete nearestPOI;
}

/**
 *
 * Here the Route is setup by connecting to to a DB
 * then adding Wayponits and POI to route
 */
void CNavigationSystem::enterRoute()
{
	/* Private member function to create the DB
	 * Connect the DB to current route
	 * Add waypoints to route
	 * Add POIs to route
	 */

	CwayPoint Berlin("Berlin",52.5167,13.4),
			Amsterdam("Amsterdam",52.3667,4.9000),
			Darmstadt("Darmstadt",49.878708,8.646927);
	CwayPoint	TOkio("TOkio",35.6833,139.6833);
	CwayPoint Dieburg("Dieburg",1.2,12.34);
	CwayPoint testwp("dummy",10,10);

	CPOI mitte(RESTAURANT,"mitte","Regular Place",49.87477, 8.656423);
	CPOI Kinopolis(KINO,"Kinopolis","best german movies",49.8717537,9.6331035);
	CPOI DB(ATM,"DB","Any Time Money",49.6,7.65);
	CPOI Luisen(CITYCENTER,"Luisen","city center of Darmstadt",45.6,8.65);
	CPOI rosenhohe(PARK,"rosenhohe","darmstadts biggest park",45.6,9.65);
	CPOI testPoi(PARK,"dummy","test test",45.6,9.65);


//	cout<<Berlin;
//	cout<<Kinopolis;

	m_route.connectToPoiDB(&m_PoiDatabase);									// Connect to DB to current Route
	m_route.connectToWpDB(&m_WpDataBase);
	// Add POIs to DB

//
//	CPoiDatabase testPoiDB;
//	CwpDatabase wpdb;

	/** Creating WP database **/
	m_WpDataBase.addWaypoint(Berlin);
	m_WpDataBase.addWaypoint(Berlin);
	m_WpDataBase.addWaypoint(CwayPoint("Berlin",52,13));
	m_WpDataBase.addWaypoint(Amsterdam);
	m_WpDataBase.addWaypoint(Darmstadt);
	m_WpDataBase.addWaypoint(TOkio);
	m_WpDataBase.addWaypoint(Dieburg);
	m_WpDataBase.addWaypoint(testwp);

	/** Creating POI database **/
	m_PoiDatabase.addPoi(CPOI(GAS,"aral","gas station",44.6,10.65));
	m_PoiDatabase.addPoi(DB);
	m_PoiDatabase.addPoi(DB);
	m_PoiDatabase.addPoi(mitte);
	m_PoiDatabase.addPoi(Kinopolis);
	m_PoiDatabase.addPoi(Luisen);
	m_PoiDatabase.addPoi(testPoi);

	m_PoiDatabase.addPoi(DB);
	m_PoiDatabase.addPoi(DB);
	m_PoiDatabase.addPoi(Luisen);

	/** Creating ROUTE  **/
	//adding waypoints
	m_route.addWaypoint("Berlin");
	m_route.addWaypoint("Berlin");
	m_route.addWaypoint("Amsterdam");
	m_route.addWaypoint("Berlin");
	m_route.addWaypoint("Berlin");

	m_route.addWaypoint("Darmstadt");
	m_route.addWaypoint("Darmstadtert");
	m_route.addWaypoint("Dieburg");
	//adding pois
	m_route.addPoi("aral","Berlin");
	m_route.addPoi("DB","Berlin");
	m_route.addPoi("DB","Berlin");
	m_route.addPoi("mitte","Darmstadt");
	m_route.addPoi("Luisen","Darmstadt");
	m_route.addPoi("Kebap","Darmstadt");
	m_route.addPoi("Kinospolis","Stsuttgart");
	m_route+="Dieburg";
	bool status;
	CJsonPersistence Readfile,writeFile,BackupFile;
	BackupFile.setMediaName("backup");
	BackupFile.writeData(m_WpDataBase,m_PoiDatabase);// write data to backup.json


//	Readfile.setMediaName("input");
//	status=Readfile.readData(m_WpDataBase,m_PoiDatabase,CPersistentStorage::REPLACE); // read data in replace mode
//	if(status==true)
//	{
//		m_route.ClearRoute(); // clear the route, data base is replaced
//	}
//
//	m_route+="luisen";	// add few places to route
//	m_route+="Amsterdam";
//	m_route+="vapiano";
	writeFile.setMediaName("output");
	status=writeFile.writeData(m_WpDataBase,m_PoiDatabase);	// write the new database to output.json




	// Uncomment the below block for other test conditions

	//testing copy constructor
/*	CRoute copyofRoute(m_route);
	cout<<"\n Printing Copy of Route\n"<<endl;
	copyofRoute.print();
	CRoute A,B;
	A.connectToPoiDB(&m_PoiDatabase);									// Connect to DB to current Route
	A.connectToWpDB(&m_WpDataBase);
	A.addWaypoint("Darmstadt");
	A.addPoi("Luisen","Darmstadt");
	cout<<"\n\n*****create a new route*********\n\n";
	A.print();
	cout<<"\n\n***** test + operator and = operator; *********\n\n";
	B=A+m_route;

	cout<<"\n\n***** Printing B *********\n\n";
	B.print();

	A+="dummy";
	A.addWaypoint("dummy");
	cout<<"\n\n***** Printing A *********\n\n";
	// File handling
	A.print();
	CfileStorage Readfile,writeFile;
	Readfile.setMediaName("NAVSYS");
	writeFile.setMediaName("NAVSYSWRITE");



	bool status=Readfile.readData(m_WpDataBase,m_PoiDatabase,Readfile.REPLACE);
	cout<<"Read Status = "<<status<<endl;
	status=writeFile.writeData(m_WpDataBase,m_PoiDatabase);
	cout<<"Write Status = "<<status<<endl;*/
	return;

}
/**
 * Prits the details of waypoints and POIs in the current route
 */
void CNavigationSystem::printRoute()

{
m_route.print(); // Print the current route
}
/**
 * The core functionality of Navigation system is coordinated here.
 * Setting up route
 * Printing the route
 * Print distance to nearest POI
 */
void CNavigationSystem::run()
{
/*
 * Coordinate the functions of the navigation system by calling the member functions.
 */
	enterRoute();
	printRoute();
	printDistanceCurPosNextPoi();


return;
}
