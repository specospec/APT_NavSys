/***************************************************************************
*============= Copyright by Darmstadt University of Applied Sciences =======
****************************************************************************/

// System Headers

#include <string>

using namespace std;


// Own Headers


// Implementation
#include "CGPSSensor.h"
#include "CwayPoint.h"

CGPSSensor::CGPSSensor()
{
	/*
	 * Default connstructor doing nothing
	 */
#ifdef DEBUGPRINTING
	extern int count_constructor;
	cout<<"constructing CGPSSensor: "<<count_constructor++<<"@"<<this<<endl;
#endif


}
/**
 *
 * @return : Returns the waypoint object with details of the current location acquired via keyboard
 */
CwayPoint CGPSSensor::getCurrentPostion()
{
	/*
	 * GPS Sensor simulated via keyboard input
	 */

	CwayPoint gpsLoc;									// Local variable to return WP
	string locName;										// Parameter to call set
	double locCurLat,locCurLong;						// Parameter to call set
	cout<<"\nGPS Sensor\nEnter the location \n";
//	cin>>locName;										// Get name from keyboard
	cout<<"\nEnter Latitude and Longitude\n";
	cin>>locCurLat>>locCurLong;							// Get coordinates from keyboard

	gpsLoc.set(locName,locCurLat,locCurLong);			// Call set function to initialize wp object

#ifdef DEBUGPRINTING

	cout<<"Displaying CGPSSensor: "<<endl;
	gpsLoc.print(DEGREE);
#endif

	return gpsLoc;										// Return object
}
